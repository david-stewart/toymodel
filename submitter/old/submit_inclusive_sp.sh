#!/bin/bash

BASEDIR=/home/rusnak/jet_analysis/toymodel
LOGDIR=$BASEDIR/submitter/log


export JETONLY=1 #only hard jets
export BOLTZMANN=0 #only soft BG

for RADIUS in 0.2 #0.3 0.4
do
export RADIUS

   if [ $RADIUS == "0.2" ]; then
      ACUT=0.09
   fi
   if [ $RADIUS == "0.3" ]; then
      ACUT=0.2
   fi
   if [ $RADIUS == "0.4" ]; then
      ACUT=0.4
   fi
	if [ $JETONLY -eq 1 ];then
		ACUT=0
      EVENT_TYPE="jetonly" # boltzman, jet_plus_bg
	else
		EVENT_TYPE="jet_plus_bg"
	fi

export ACUT
echo "R: $RADIUS, area cut: $ACUT"
export PTCUT=0.2
export BKGDPTCUT=0.2
export PYTHIAHARDJET=1 #use pythia jets with pTleading cut as the hard jet distribution

TYPE="1M_charged_R$RADIUS"

START=0
MAX=20

export COLLIDER="RHIC_CHARGED"
export NEVENTS=5E4
#VecMult=( 2000 1680 1370 1120 900 740 590 470 370 290 210 160 )
VecMult=( 1333 1120 913 747 600 493 393 313 247 193 140 107 )
VecNbin=(  984  787  619  488 380 293 222 166 120  87  64  44 )

#export COLLIDER="LHC_CHARGED"
#export NEVENTS=1E4
#VecMult=( 4800 3880 2900 1950 1278 783 447 228 )
#VecNbin=( 1259  981  676  405  230 121  59  27 )

for((INDEX=0; INDEX < 1; INDEX++))
  do
  export NBIN=${VecNbin[INDEX]}
  export SIGMA_NBIN=0
  
  export MULTIPLICITY=${VecMult[INDEX]}
  export SIGMA_MULTIPLICITY=0
  
  for((run=START; run < MAX; run++))
    do
    export NAME="sptoy${COLLIDER}${run}_${TYPE}"
    export OUTPUTDIR="${BASEDIR}/DataOut/sp/${EVENT_TYPE}/${TYPE}/${run}"
   
    if [ ! -e $OUTPUTDIR ]; then
	mkdir -p $OUTPUTDIR
    fi

    qsub -l eliza17io=1 -m a -M rusn@email.cz -N $NAME -e $LOGDIR -o $LOGDIR -V run_inclusive_sp.sh
    #run_inclusive_sp.sh
  done
done
done
