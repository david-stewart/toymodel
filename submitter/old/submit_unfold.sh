#!/bin/bash
SCRIPT_NAME=`basename -- $0`
_usage() {
    echo "Usage: ${SCRIPT_NAME} RMATRIX_TYPE (BG_sp | BG_dete | dete | effi)" 
    exit 1
}

RMATRIX_TYPE=$1  #BG_sp BG_pyt dete BG_dete - correction for BG (using single particle / pythia jet), detector effects, BG+detector effects
echo "TYPE: $RMATRIX_TYPE"

#check arguments
[ -n "$RMATRIX_TYPE" ] || _usage


#prior_type=(truth flat pythia powlaw3 powlaw45 powlaw5 powlaw55 levy levy_alex )
prior_type=(flat flat pythia powlaw4 powlaw45 powlaw5 powlaw55 tsalis_1 tsalis_2 tsalis_3 tsalis_4 tsalis_5 tsalis_6 tsalis_7 tsalis_8 tsalis_9)
export PTCUT=0.2
export NBINS=VAR
COLLIDER="RHIC" # "LHC" # 
JET_TYPE="pythia" # sp | pythia
SCRIPTNAME="run_unfold_allpTlead_allpriors.sh"
# FOR PRIOR DISTRIBUTION
export NITER=10 #number of k-terms
export BINCONTCUT=10 #minimal bin content in measured distribution
EFFICORR=1 #do efficiency correction
export SECONDUNFOLD=0 #unfold already unfolded results (eg. using a different RM)
export INPUTITER=5 #if unfolding already unfolded results, which iteration to unfold
export EFFI_UNFOLD=0 #for jet reconstruction efficiency calculation
export DIRSUF="_eta_dete" #output directory suffix

export PRIORS="2 4 5 6 7 8 9 10 11 12 13 14 15"

for CENTRAL in 1 #central/peripheral collisions
do
export CENTRAL

if [ $CENTRAL -eq 1 ]; then
	CENTSUFF=""
	PTLEADCUTS="5 6 7"
	RAA_TOY=0.5 #RAA value used for generating the simulated spectrum - used only for desctription
else
	CENTSUFF="_peripheral"
	PTLEADCUTS="4 5 6"
	RAA_TOY=0.7 #RAA value used for generating the simulated spectrum - used only for desctription
fi
export PTLEADCUTS
export RAA_TOY

for SVD in 0 1 # unfolding type: 0 - Bayes | 1 - SVD
do
export SVD
if [ $SVD -eq 1 ]; then
   UNFTYPE="SVD"
else
   UNFTYPE="Bayes"
fi
export UNFTYPE

if [ $EFFI_UNFOLD -eq 1 -o $RMATRIX_TYPE == "BG_sp" ]; then
   export EFFICORR=0
else
   export EFFICORR
fi

if [ $EFFICORR -eq 0 ]; then
EFFSUF=""
else
EFFSUF="_eff"
fi

for BININGCH in 1 2 3 4 #1 2 3 #choice of bining arrays 0: nu=nm, 1: nu<nm, 2: nu<nm
do
export BININGCH 

for RPARAM in 0.3 #0.2 0.4 
do
export RPARAM
TOYMODELPATH="$HOME/jet_analysis/toymodel"
WRKDIR="$TOYMODELPATH/DataOut/$JET_TYPE/jet_plus_bg/charged_R${RPARAM}${CENTSUFF}"
export TRUE_PATH="$TOYMODELPATH/DataOut/$JET_TYPE/jetonly/charged_R${RPARAM}${CENTSUFF}"
export PRIOR_PATH="~/jet_analysis/STARJet/out/MB/prior"
export DATA_PATH=$WRKDIR
export RMATRIX_PATH="$WRKDIR/rmatrix"
export RMATRIX_TYPE
export EPSILON_PATH="$HOME/jet_analysis/toymodel/DataOut/pythia/jetonly/pyEmb_R${RPARAM}${CENTSUFF}_normal/epsilon"
LOGDIR="$TOYMODELPATH/submitter/log"

#for PRIOR in 2 4 5 6 7 8 9 10 11 12 13 14 15
#do
   #OUT_DIR=$DATA_PATH"/Unfolded_R${RPARAM}_SVD_${NBINS}bins_bining${BININGCH}_${RMATRIX_TYPE}_RAA${RAA}${SUF}/"${prior_type[$PRIOR]}
   #echo "creating directory: $OUT_DIR"
   #rm  $OUT_DIR/*.root 
   #mkdir -p $OUT_DIR
	#export OUT_DIR
   #export PRIOR
   #for PTTHRESH in `echo $PTLEADCUTS`
   #do
     #export PTTHRESH
     NAME="toyunf${UNFTYPE}_S${CENTRAL}_B${BININGCH}_R${RPARAM}"

		qsub -P star -m n -l h_vmem=1G -l h_rt=5:00:00 -N $NAME -o $LOGDIR -e $LOGDIR -V $SCRIPTNAME 
	#done #prior
#done #pTlead
done #R
done #bining
done #centrality
done #unfolding
