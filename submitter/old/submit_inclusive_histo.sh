#!/bin/bash

export BASEDIR=$HOME/jet_analysis/toymodel
LOGDIR=$BASEDIR/submitter/log

MEVENTS=40 #in millions
export COLLIDER="RHIC_CHARGED" # RHIC | RHIC_CHARGED | LHC
JET_TYPE="pythia" # pythia | sp - produce pythia jets or single particle jets
#JET_TYPE="sp" # pythia | sp - produce pythia jets or single particle jets
export RADIUS=0.4
export PTCUT=0.2
export JETONLY=0 #only hard jets, if JETONLY=0 and BOLTZMAN=0 => generate hard jets + BG
export BOLTZMANN=0 #only soft BG
export BKGDPTCUT=0.2 #not used anymore
export PYTHIAHARDJET=1 #use pythia jets with pTleading cut as the hard jet distribution, otherwise pT^-6 spectrum will be used
export EFFICORR=1 #simulate tracking inefficiency
export PTSMEAR=1 #track pT smearing
export EFFTYPE="pp" #tracking efficiency model: pp | AuAu - like
export EFFPATH="$HOME/jet_analysis/STARJet/analysis/efficiency" #tracking efficiency model: pp | AuAu - like
export JETFRAG="u" #jet fragmentation: u | g

TYPE4="" #output directory suffix


	if [ $JETONLY -eq 1 ]; then
		EVENT_TYPE="jetonly"
	elif [ $BOLTZMANN -eq 1 ]; then
		EVENT_TYPE="boltzman"
	else
		EVENT_TYPE="jet_plus_bg" 
	fi

   if [ $RADIUS == "0.2" ]; then
      ACUT=0.09
   fi
   if [ $RADIUS == "0.3" ]; then
      ACUT=0.2
   fi
   if [ $RADIUS == "0.4" ]; then
      ACUT=0.4
   fi
if [[ $JETONLY -eq 1 && $EFFICORR -eq 0 ]]; then
	ACUT=0
fi
export ACUT
echo "Area cut: $ACUT"

	if [ $PYTHIAHARDJET -eq 1 ]; then
	TYPE1="${MEVENTS}M_charged_R${RADIUS}_A${ACUT}_pyjet"
	else
	TYPE1="${MEVENTS}M_charged_R${RADIUS}_A${ACUT}_powlaw"
	fi

	if [ $EFFICORR -eq 1 ]; then
	TYPE2="_effcorr"
	else
	TYPE2=""
	fi

	if [ $PTSMEAR -eq 1 ]; then
	TYPE3="_pTsmear"
	else
	TYPE3=""
	fi

	TYPE=${TYPE1}${TYPE2}${TYPE3}${TYPE4}

	export NEVENTS=100000
	START=100
	MAX=$(( MEVENTS * 1000000 / NEVENTS ))
	#START=75
	#MAX=76
	echo "number of jobs: $MAX"

	#VecMult=( 2000 1680 1370 1120 900 740 590 470 370 290 210 160 )
	VecMult=( 1333 1120 913 747 600 493 393 313 247 193 140 107 )
	VecNbin=(  984  787  619  488 380 293 222 166 120  87  64  44 )

	for((INDEX=0; INDEX < 1; INDEX++))
	  do
	  export NBIN=${VecNbin[INDEX]}
	  export SIGMA_NBIN=0
  
	  export MULTIPLICITY=${VecMult[INDEX]}
	  export SIGMA_MULTIPLICITY=0
  
	  for((run=START; run < MAX; run++))
   	 do
	    export NAME="sptoy${COLLIDER}${run}_${TYPE}"
	    export OUTPUTDIR="${BASEDIR}/DataOut/${JET_TYPE}/${EVENT_TYPE}/${TYPE}/${run}"
 
  	    if [ ! -e $OUTPUTDIR ]; then
			mkdir -p $OUTPUTDIR
	    fi

   	 #qsub -l projectio=1 -m n -N $NAME -e $LOGDIR -o $LOGDIR -V run_inclusive_${JET_TYPE}_histo.sh
	    ./run_inclusive_pythia_histo.sh
	  done
	done

