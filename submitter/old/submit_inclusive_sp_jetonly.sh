#!/bin/bash

BASEDIR=/home/rusnak/jet_analysis/toymodel
LOGDIR=$BASEDIR/submitter/log

TYPE="100k_charged_R04_pyt_A0"
EVENT_TYPE="jetonly" #jetonly, boltzman, jet_plus_bg
export ACUT=0.0
#export ACUT=0.4
export PTCUT=0.2
export RADIUS=0.4
export JETONLY=1 #only hard jets
export BOLTZMANN=0 #only soft BG
export BKGDPTCUT=0.2
export PYTHIAHARDJET=1 #use pythia jets with pTleading cut as the hard jet distribution

START=0
MAX=10

export COLLIDER="RHIC_CHARGED"
export NEVENTS=1E4
#VecMult=( 2000 1680 1370 1120 900 740 590 470 370 290 210 160 )
VecMult=( 1333 1120 913 747 600 493 393 313 247 193 140 107 )
VecNbin=(  984  787  619  488 380 293 222 166 120  87  64  44 )

#export COLLIDER="LHC_CHARGED"
#export NEVENTS=1E4
#VecMult=( 4800 3880 2900 1950 1278 783 447 228 )
#VecNbin=( 1259  981  676  405  230 121  59  27 )

for((INDEX=0; INDEX < 1; INDEX++))
  do
  export NBIN=${VecNbin[INDEX]}
  export SIGMA_NBIN=0
  
  export MULTIPLICITY=${VecMult[INDEX]}
  export SIGMA_MULTIPLICITY=0
  
  for((run=START; run < MAX; run++))
    do
    export NAME="sptoy${COLLIDER}${run}_${TYPE}"
    export OUTPUTDIR="${BASEDIR}/DataOut/sp/${EVENT_TYPE}/${TYPE}/${run}"
   
    if [ ! -e $OUTPUTDIR ]; then
	mkdir -p $OUTPUTDIR
    fi

    qsub -l eliza17io=1 -m a -M rusn@email.cz -N $NAME -e $LOGDIR -o $LOGDIR -V run_inclusive_sp.sh
    #run_inclusive_sp.sh
  done
done

