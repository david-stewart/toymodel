#ifndef __UnfoldBayes__hh
#define __UnfoldBayes__hh
 
#include "Rtypes.h"
#include "TString.h"
#include "TVectorD.h"
#include "TMatrixD.h"

class TH1D;
class TH2D;
class TFile;

class UnfoldBayes
{
 public:
  UnfoldBayes(Int_t niterations, TString outfile);
  ~UnfoldBayes();

  void Unfold();
  void SetSmooth() {kSmooth = kTRUE;}
  void SetSmoothRange(Double_t xmin, Double_t xmax) {fXmin = xmin; fXmax = xmax;}
  void SetHistograms(TH1D *measured, TH1D *prior, TH2D *smearing);
  void SetHistograms(TH1D *truth, TH1D *measured, TH1D *prior, TH2D *smearing);

 private:
  void SetMPEC();
  void SetVPrior();
  void SetVMeasured();
  void SetDimensions();

  void UpdatePrior();
  void FillPECPrior();  
  void SaveResults(Int_t iteration);

  void MakeHistogram(TH1D *hist);
  void MakeUnfoldingMatrix();
  void MakeCovarianceMatrix();
  void MakeCorrelationMatrix();

 protected:	
  TFile *fout;

  TVectorD VPrior;        // Prior distribution
  TVectorD VMeasured;     // Measured distribution
  TVectorD VUnfolded;     // Unfolded distribution
  
  TMatrixD MPEC;          // P(Ej | Ci, I)
  TMatrixD MPCE;          // P(Ci | Ej, I)

  TMatrixD MCovUnfoldMult; // Covariance matrix
  TMatrixD MCovUnfoldPois; // Covariance matrix
  TMatrixD MCovMeasMult;   // Covariance matrix cov(nEi, nEj) assuming multinomial distribution
  TMatrixD MCovMeasPois;   // Covariance matrix cov(nEi, nEj) assuming poisson distribution

  TH1D *HPrior;
  TH1D *HTruth;
  TH1D *HMeasured;
  TH1D *HUnfolded;

  TH2D *HPECPrior;
  TH2D *HSmearing;
  TH2D *HUnfolding;

  TH2D *HCovUnfoldMult;
  TH2D *HCovUnfoldPois;

  TH2D *HCorrUnfoldMult;
  TH2D *HCorrUnfoldPois;

  Int_t fncauses;
  Int_t fneffects;
  Int_t fiteration;
  Int_t fniterations;

  Bool_t kSmooth;

  Double_t fXmin;
  Double_t fXmax;
};

#endif
