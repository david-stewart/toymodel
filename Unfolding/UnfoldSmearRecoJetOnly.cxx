#include "TF1.h"
#include "TH1D.h"
#include "TH2D.h"
#include "TFile.h"
#include "TMath.h"
#include "TRandom.h"
#include "TString.h"
#include "TStopwatch.h"

#include "Riostream.h"

#include "UnfoldBayes.h"
#include "UnfoldFolding.h"
#include "UnfoldSmearRecoJetOnly.h"

void SmearRecoJetOnly(Double_t pTcut, Double_t pTthresh)
{
  TStopwatch timer;
  timer.Start();

  TString str;

  // DELTA PT
  str = Form("~/Doutorado/toymodel/data/Mult8246_Nbin1259/low_stat_deltapT_R0.4_pTcut%.1lf.root", pTcut);
  TFile *fdeltapT = new TFile(str.Data(), "OPEN");
  TH1D *hdpT[4];
  Float_t pTemb[] = {1.0, 5.0, 10.0, 15.0};

  for(Int_t iemb = 0; iemb < 4; iemb++)
    {
      hdpT[iemb] = (TH1D*)fdeltapT->Get(Form("hdpt_pTemb%.1lf_pTleading%.1lf", pTemb[iemb], pTcut));

      hdpT[iemb]->Rebin(10);

      if(hdpT[iemb]->Integral() != 1.0)
	hdpT[iemb]->Scale(1./hdpT[iemb]->Integral());

      cout << Form("pTemb = %.1lf \t Integral = %.3e", pTemb[iemb], hdpT[iemb]->Integral()) << endl;
    }

  // RECONSTRUCTED JETS HARD PIECE ONLY
  str = Form("~/Doutorado/toymodel/data/Mult0_Nbin1259/low_stat_histos_jets_R0.4_pTcut%.1lf.root", pTthresh);
  TFile *fjets = new TFile(str.Data(), "OPEN");
  TH1D *hPtReco = (TH1D*)fjets->Get("hPtReco");
  hPtReco->Rebin(10);

  Int_t nbins = hPtReco->GetNbinsX();
  Double_t xmin = hPtReco->GetBinLowEdge(1);
  Double_t xmax = hPtReco->GetBinLowEdge(nbins+1);

  // RESPONSE MATRIX
  str = Form("~/Doutorado/toymodel/data/Mult8246_Nbin1259/low_stat_response_matrix_deltapT_uniform_pTcut%.1lf.root", pTcut);
  TFile *frespmatrix = new TFile(str.Data(), "OPEN");
  TH2D *hrespmatrix = (TH2D*)frespmatrix->Get("hResponse_1E9");
  TH2D *hresptemp = new TH2D("hresptemp", "hresptemp", nbins, xmin, xmax, nbins, xmin, xmax);
  for(Int_t binx = 1; binx <= hrespmatrix->GetNbinsX(); binx++)
    for(Int_t biny = 1; biny <= hrespmatrix->GetNbinsY(); biny++)
      {
	Double_t yield = hrespmatrix->GetBinContent(binx, biny);
	Double_t x = hrespmatrix->GetXaxis()->GetBinCenter(binx);
	Double_t y = hrespmatrix->GetYaxis()->GetBinCenter(biny);
	hresptemp->Fill(x, y, yield);
      }

  // ANALYSIS 
  str = Form("~/Doutorado/toymodel/data/Mult0_Nbin1259/low_stat_recojets_smeared_pTcut%.1lf.root", pTthresh);
  TFile *foutput = new TFile(str.Data(), "RECREATE");

  //  SMEARING
  TH1D *hsmear;
  TH1D *hsmeared = new TH1D("hsmeared","hsmeared", nbins, xmin, xmax);
  hsmeared->Sumw2();
  
  FoldWithDeltaPt(hPtReco, hdpT, hsmeared);
  
  // UNFOLDING
  TH1D *hprior = (TH1D*)hPtReco->Clone("hprior");
  str = Form("~/Doutorado/toymodel/data/Mult0_Nbin1259/low_stat_unfolded_recojets_smeared_pTcut%.1lf.root", pTthresh);
  Int_t niterations = 5;
  UnfoldBayes *bayes = new UnfoldBayes(niterations, str.Data());
  bayes->SetHistograms(hsmeared, hprior, hresptemp);
  bayes->Unfold();

  foutput->cd();
  hsmeared->Write();
  hPtReco->Write();
  foutput->Close();

  fjets->Close();
  frespmatrix->Close();
  fdeltapT->Close();

  timer.Stop();
  timer.Print();
}

