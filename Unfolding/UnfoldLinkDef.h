#ifdef __CINT__
 
#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;

#pragma link C++ function TestPlaw ;
#pragma link C++ function FoldWithDeltaPt ;
#pragma link C++ function SmearRecoJetOnly ;
#pragma link C++ function CompareModelSmearing ;

#pragma link C++ class TH2DTools++ ;
#pragma link C++ class UnfoldBayes++ ;
#pragma link C++ class UnfoldSVD++ ;
//#pragma link C++ class UnfoldBuildResponseMatrixSVD++ ;
#pragma link C++ class UnfoldBuildResponseMatrixROO++ ;
#pragma link C++ class UnfoldBuildResponseMatrix++ ;

#endif
