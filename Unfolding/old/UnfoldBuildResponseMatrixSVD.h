#ifndef __UnfoldBuildResponseMatrixSVD__hh
#define __UnfoldBuildResponseMatrixSVD__hh

#include "TString.h"

class TH1D;
class TH2D;
class TFile;

class UnfoldBuildResponseMatrixSVD
{
 public:
  UnfoldBuildResponseMatrixSVD(TString path, Float_t R, Float_t pTthresh, Int_t priorNo);
  ~UnfoldBuildResponseMatrixSVD();
  
  void BuildDeltaPtResponseMatrix();
  
 private:
  Double_t SmearWithDeltaPt(Double_t pT);

 protected:
  TFile *fout;
  TFile *finput;

  TH1D *hdpT[13];
  TH2D *hResponse;
  TH1D *hMCtrue;
  TH1D *hMCreco;
  TH1D *hprior;

  Int_t nbins;
  Int_t nevts;
  
  Double_t pTmin;
  Double_t pTmax;

  TString str;

  static const Int_t nEmb=7;
};

#endif
