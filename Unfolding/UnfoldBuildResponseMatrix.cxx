#include "TF1.h"
#include "TH2D.h"
#include "TFile.h"
#include "TMath.h"
#include "TRandom.h"
#include "TString.h"
#include "TSystem.h"
#include "Riostream.h"
#include "TDirectoryFile.h"
 
#include "UnfoldBuildResponseMatrix.h"

//==============================================================================
UnfoldBuildResponseMatrix::UnfoldBuildResponseMatrix()
{
  nbins = 800;
  nevts = 1E9;

  pTmin = 0.0;
  pTmax = 100.;

  hResponse = 0x0;
  
  fout = new TFile("response_matrix_gaussian.root", "RECREATE");
}

//=============================================================================
UnfoldBuildResponseMatrix::UnfoldBuildResponseMatrix(TString path, Float_t R, Float_t pTleading)
{
  nbins = 800;
  nevts = 1E9;

  pTmin = pTleading;
  pTmax = 100.;

  hResponse = 0x0;
  hntrue=new TH1D("hntrue", "n true generated;p_{T}^{true};entries", nbins, -pTmax, +pTmax);
  
  finput = new TFile(Form("%s/histos_dpTarea_R%.1lf_pTcut0.2.root", path.Data(), R), "OPEN");

  //Float_t pTemb[] = {0.1, 1.0, 2.0, 3.0, 5.0, 10.0, 15.0};
  Float_t pTemb[] =  {0.5, 1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0, 10.0, 12.0, 15.0};
  for(Int_t idist = 0; idist < nEmb; idist++)
    {
      TString name = Form("hdpT_pTlead_Acut_pTemb%.1lf", pTemb[idist]);
		TH2D* htmp= (TH2D*)finput->Get(name.Data());
		
		name=Form("dpt_pTl%.0lf_emb%i",pTleading,idist);
		Int_t firstbin=htmp->GetYaxis()->FindBin(pTleading);
		Int_t lastbin=htmp->GetYaxis()->GetNbins();
      hdpT[idist] = (TH1D*)htmp->ProjectionX(name,firstbin,lastbin);
		delete htmp;
      // removing 1 +/- 1
      for(Int_t bin = 1; bin <= 2000; bin++)
	if(hdpT[idist]->GetBinContent(bin) == hdpT[idist]->GetBinError(bin))
	  {
	    hdpT[idist]->SetBinContent(bin, 0);
	    hdpT[idist]->SetBinError(bin, 0);
	  }
    }

  fout = new TFile(Form("%s/rmatrix/response_matrix_BG_sp_R%.1lf_pTlead%.0lf.root", path.Data(), R, pTleading), "RECREATE");
}

//==============================================================================
UnfoldBuildResponseMatrix::~UnfoldBuildResponseMatrix()
{
  fout->Close();
  delete fout;
}

//==============================================================================
void UnfoldBuildResponseMatrix::BuildGaussianResponseMatrix()
{
  TString name;
  Double_t sigma = 4.;
  Int_t save_evt = 1;

  hResponse = new TH2D("hResponse", "hResponse;p_{T}^{meas};p_{T}^{true};entries", nbins, -pTmax, +pTmax, nbins, -pTmax, +pTmax);

  for(Int_t ievt = 0; ievt < nevts; ievt++)
    {
      Double_t pT = gRandom->Uniform(pTmin, pTmax);
      Double_t dpT = gRandom->Gaus(0, sigma);
      hResponse->Fill(pT + dpT, pT);

      if(ievt != TMath::Power(10, save_evt) - 1) continue;
      name = Form("hResponse_1E%d", save_evt);
      fout->cd();
      hResponse->Write(name.Data());
      cout << Form("%s saved!", name.Data()) << endl;
      save_evt++;
    }

  fout->cd();
  hResponse->Write();
  delete hResponse;
}

//==============================================================================
void UnfoldBuildResponseMatrix::BuildDeltaPtResponseMatrix()
{
  TString name;
  Int_t save_evt = 1;

  hResponse = new TH2D("hResponse", "hResponse;p_{T}^{meas};p_{T}^{true};entries", nbins, -pTmax, +pTmax, nbins, -pTmax, +pTmax);
  
  for(Int_t ievt = 0; ievt < nevts; ievt++)
    {
      Double_t pT = gRandom->Uniform(pTmin, pTmax);
      Double_t dpT = SmearWithDeltaPt(pT);
      hResponse->Fill(pT + dpT, pT);
		hntrue->Fill(pT);

		if(ievt==TMath::Power(10, save_evt) )
      {
         cout <<"doing event 1E"<<save_evt<<endl;
         save_evt++;
      }

		/*
      if(ievt != TMath::Power(10, save_evt) - 1) continue;
      name = Form("hResponse_1E%d", save_evt);
      fout->cd();
      hResponse->Write(name.Data());
      cout << Form("%s saved!", name.Data()) << endl;
      save_evt++;*/
    }
  
//normalize matrix
for(int j=1; j<nbins+1; j++){
      double ntot=hntrue->GetBinContent(j);
   for(int i=1; i<nbins+1; i++){
      double binc = hResponse->GetBinContent(i,j);
		if(binc<1 || ntot<1)continue;
      double prob=binc/ntot;
      hResponse->SetBinContent(i,j,prob);
   }//j - true
}//i - measured

  fout->cd();
  hResponse->Write("hResponse_1E9");
  hntrue->Write("hntrue");

  delete hResponse;
}

//==============================================================================
Double_t UnfoldBuildResponseMatrix::SmearWithDeltaPt(Double_t pT)
{
  Float_t fEmbPt[] =  {0.5, 1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0, 10.0, 12.0, 15.0};
   Double_t dpT = 0;
  if (pT <= fEmbPt[0]){
    dpT = hdpT[0]->GetRandom();
   }
  for(int i=0; i<(nEmb-1); i++){
   if(pT>fEmbPt[i] && pT<=fEmbPt[i+1]){
   Double_t rnd=gRandom->Uniform(fEmbPt[i],fEmbPt[i+1]);
   if(pT>rnd) dpT = hdpT[i+1]->GetRandom();
   else dpT = hdpT[i]->GetRandom();
   }
  }
  if(pT>fEmbPt[nEmb-1]) {
   dpT = hdpT[nEmb-1]->GetRandom();
}
  return dpT;

/*
//-------------------------
  Double_t dpT = 0.0;
	  
  if(pT < 1.0)
    dpT = hdpT[0]->GetRandom();
  else
  if(pT < 2.0)
    dpT = hdpT[1]->GetRandom();
  else
  if(pT < 3.0)
    dpT = hdpT[2]->GetRandom();
  else
  if(pT < 5.0)
    dpT = hdpT[3]->GetRandom();
  else
  if(pT < 10.0)
	 dpT = hdpT[4]->GetRandom();
  else
  if(pT < 15.0)
    dpT = hdpT[5]->GetRandom();
  else
    dpT = hdpT[6]->GetRandom();
  return dpT;
  */
}
