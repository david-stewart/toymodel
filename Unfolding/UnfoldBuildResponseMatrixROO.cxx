#include "TF1.h"
#include "TH2D.h"
#include "TFile.h"
#include "TMath.h"
#include "TRandom.h"
#include "TString.h"
#include "TSystem.h"
#include "Riostream.h"
#include "TDirectoryFile.h"
 
#include "UnfoldBuildResponseMatrixROO.h"

//=============================================================================
UnfoldBuildResponseMatrixROO::UnfoldBuildResponseMatrixROO(Float_t R, Float_t pTthresh, Int_t priorNo, TString type)
{
  path=gSystem->Getenv("RM_PATH");
  pyEmb_path = gSystem->Getenv("PYEMB_PATH");
  prior_path= gSystem->Getenv("PRIOR_PATH");
  true_path= gSystem->Getenv("TRUE_PATH");

  priorN=priorNo;
  pTlead=pTthresh;

  nbins = 800;
  nevts = 1E9;

  pTmax = 100.;
  pTmin = -pTmax;
	if(type=="effi"){
		pTmin=0;
		nbins=400;
	}
  
  hResponse = 0x0;
  mtype=type;

  //TString prior_type[]={"truth","flat","pythia","powlaw3","powlaw45","powlaw5","powlaw55","levy_jan","levy_alex"};
  TString prior_type[]={"","flat","pythia","powlaw4","powlaw45","powlaw5","powlaw55","tsalis_1","tsalis_2","tsalis_3","tsalis_4","tsalis_5","tsalis_6","tsalis_7","tsalis_8","tsalis_9"};
  str=Form("%s/response_matrix_%s_R%.1lf_pTlead%.0lf.root", path.Data(),mtype.Data(),R,pTthresh);
  if(mtype=="effi" || mtype=="dete")str=Form("%s/pythia_emb_R%.1lf.root",pyEmb_path.Data(),R);
  finput = new TFile(str, "OPEN");

      TString name = "hResponse_1E9"; //use "classical" RM as input
		if(mtype=="effi" || mtype=="dete") name = Form("hresponse_pTl%.0lf",pTthresh);
       hRMin = (TH2D*)finput->Get(name.Data());

	Int_t binzero=hRMin->GetYaxis()->FindBin(0.0);
	for(Int_t i=binzero; i<hRMin->GetNbinsY()+1; i++){
	   Int_t bin = i;
   	TString name=Form("hdpT_%i",i);
   	hdpT[i-binzero]=(TH1D*)hRMin->ProjectionX(name, bin, bin);
	}
  str = Form("%s/response_matrix_%s_R%.1lf_pTlead%.1lf_prior%i.root", path.Data(),mtype.Data(),R,pTthresh,priorNo);
  if(mtype=="effi")str= Form("%s/rmatrix/response_matrix_%s_R%.1lf_pTlead%.1lf_prior%i.root", pyEmb_path.Data(),mtype.Data(),R,pTthresh,priorNo);
  fout = new TFile(str, "RECREATE");


//seting up prior distribution
if (priorNo==0){
str=Form("%s/histos_jets_R%.1lf_pTcut0.2.root",true_path.Data(),R);
cout<<"opening file: "<<str.Data()<<endl;
TFile *fprior = new TFile(str.Data(), "OPEN");
TH2D *hPtRecpTleadingPrior = (TH2D*)fprior->Get("fhPtRecpTleading");
  Int_t firstbin = hPtRecpTleadingPrior->GetXaxis()->FindBin(pTthresh);
  Int_t lastbin = hPtRecpTleadingPrior->GetNbinsX();
  hprior = hPtRecpTleadingPrior->ProjectionY("hprior", firstbin, lastbin);
}
else{
  str = Form("%s/priors_default.root", prior_path.Data());
  TFile *fpriorfile = new TFile(str.Data(), "OPEN");
  str=Form("%s",prior_type[priorNo].Data());
  if(priorNo==2) str=Form("%s_R%.0lf_pTlead%.0lf",prior_type[priorNo].Data(),R*10,pTthresh);
  //if(priorNo==2) str=Form("%s_R%.0lf_pTlead5",prior_type[priorNo].Data(),R*10); //we have only fits to pythia w/ pTleading>5GeV
  fprior = (TF1*)fpriorfile->Get(str.Data());

  //str=Form("%s_R%.0lf_pTlead%.0lf","pythia",R*10,pTthresh);
  //fpythia = (TF1*)fpriorfile->Get(str.Data());
  }
}


//==============================================================================
UnfoldBuildResponseMatrixROO::~UnfoldBuildResponseMatrixROO()
{
  fout->Close();
  delete fout;
}

//==============================================================================
void UnfoldBuildResponseMatrixROO::BuildDeltaPtResponseMatrix()
{
  TString name;

  hResponse = new TH2D("hResponse", "hResponse;p_{T}^{meas};p_{T}^{true};entries", nbins, pTmin, +pTmax, nbins, pTmin, +pTmax);
  hMCtrue = new TH1D("hMCtrue", "hMCtrue;p_{T}^{true};entries", nbins, pTmin, +pTmax);
  hMCreco = new TH1D("hMCreco", "hMCreco;p_{T}^{reco};entries", nbins, pTmin, +pTmax);
 
   for(Int_t ievt = 0; ievt < nevts; ievt++){
   if(ievt%1000000==0) cout<<"EVENT:"<<ievt<<endl;

      Double_t pT=gRandom->Uniform(0,pTmax); 
      if(pT<=pTlead) continue;
		Double_t scale;
		if(priorN==0)
		 scale=hprior->GetBinContent(hprior->GetXaxis()->FindBin(pT));
		else
       scale = fprior->Eval(pT);
		//Double_t scale_pyt = fpythia->Eval(pT);
      if(scale!=scale) continue; //fprior is not defined for this pT
		//if(scale_pyt!=scale_pyt) continue; //fpythia is not defined for this pT

      Double_t dpT = SmearWithDeltaPt(pT);
      hMCtrue->Fill(pT,scale);
//      hResponse->Fill(dpT, pT,scale_pyt); //scale response matrix following the PYTHIA
      hResponse->Fill(dpT, pT,scale); 
      //hResponse->Fill(dpT2, pT2);
      hMCreco->Fill(dpT, scale);
      }//event loop

      Int_t save_evt = 9;
      fout->cd();
      name = Form("hResponse_1E%d", save_evt);
      hResponse->Write(name.Data());
      name = Form("hMCtrue_1E%d", save_evt);
      hMCtrue->Write(name.Data());
      name = Form("hMCreco_1E%d", save_evt);
      hMCreco->Write(name.Data());
      cout << Form("Event 1E%d saved!", save_evt) << endl;

 
  delete hResponse;
  delete hMCtrue;
  delete hMCreco;
}

//==============================================================================
Double_t UnfoldBuildResponseMatrixROO::SmearWithDeltaPt(Double_t pT)
{
   Double_t dpT = 0;
   Int_t  bin = hRMin->GetYaxis()->FindBin(pT);
   Int_t binzero=hRMin->GetYaxis()->FindBin(0.0);
  if (pT < 0)dpT=0;
  else dpT = hdpT[bin-binzero]->GetRandom();
  return dpT;

}
