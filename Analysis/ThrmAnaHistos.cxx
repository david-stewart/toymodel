#include "TH1D.h"
#include "TH2D.h"
#include "TFile.h"
#include "TMath.h"
#include "TNtuple.h"
#include "TString.h"

#include "ThrmAnaHistos.h"

//_____________________________________________________________________________
void RHICRecoilJetSpectrum(TString wrkdir)
{
  TString str;

  const Int_t ntrig = 2;
  Float_t pTtrigMin[] = { 5.0, 10.0};
  Float_t pTtrigMax[] = {10.0, 15.0};
  
  Float_t pT;
  Float_t rho;
  Float_t Area;
  Float_t nevent;
  Float_t pTleading;

  str = Form("%s/recoil.root", wrkdir.Data());
  TFile *foutput = new TFile(str.Data(), "RECREATE");

  for(Int_t itrig = 0; itrig < ntrig; itrig++)
    {
      foutput->cd();
      str = Form("%.1lfpTtrig%.1lf", pTtrigMin[itrig], pTtrigMax[itrig]);
      TH1D *hrecoil = new TH1D(str.Data(), "hrecoil;p_{T, corr}^{recoil jet} (GeV/c)", 400, -200, +200);
      hrecoil->Sumw2();

      str = Form("%.1lfpTtrig%.1lf_pTleading", pTtrigMin[itrig], pTtrigMax[itrig]);
      TH2D *hrecoilpTleading = new TH2D(str.Data(), "hrecoil;p_{T}^{leading} (GeV/c);p_{T, corr}^{recoil jet} (GeV/c)", 400, -200, +200, 400, -200, +200);
      hrecoilpTleading->Sumw2();

      str = Form("%s/%.0lfpTtrig%.0lf/ana_data_jets_R0.4_pTcut0.2.root", wrkdir.Data(), pTtrigMin[itrig], pTtrigMax[itrig]);
      TFile *finput = new TFile(str.Data(), "OPEN");
      TNtuple *ntuple = (TNtuple*)finput->Get("jets");
      ntuple->SetBranchAddress("pT", &pT);
      ntuple->SetBranchAddress("rho", &rho);
      ntuple->SetBranchAddress("Area", &Area);
      ntuple->SetBranchAddress("nevent", &nevent);
      ntuple->SetBranchAddress("pTleading", &pTleading);

      Int_t nentries = ntuple->GetEntries();

      for(Int_t entry = 0; entry < nentries; entry++)
	{
	  ntuple->GetEntry(entry);

	  Double_t pTcorr = pT - rho*Area;
	  
	  hrecoil->Fill(pTcorr);
	  hrecoilpTleading->Fill(pTleading, pTcorr);
 	}

      foutput->cd();

      hrecoil->GetYaxis()->SetTitle("N_{jets}");
      hrecoil->Write();
      hrecoilpTleading->Write();

      hrecoil->Scale(1./(1.2*TMath::Pi()/2.0*1E5), "width");
      str = Form("%s_normalized", hrecoil->GetName());
      hrecoil->GetYaxis()->SetTitle("1/N_{trigger}1/#Delta#phid^{2}N_{jets}/d#etadp_{T} (GeV/c)^{-1}");
      hrecoil->Write(str.Data());

      delete hrecoil;
      delete hrecoilpTleading;

      delete ntuple;
      finput->Close();
      delete finput;
    }
}

//_____________________________________________________________________________
void LHCRecoilJetSpectrum(TString wrkdir)
{
  TString str;

  const Int_t ntrig = 3;
  Float_t pTtrigMin[] = {15.0, 20.0, 25.0};
  Float_t pTtrigMax[] = {20.0, 25.0, 30.0};
  
  Float_t pT;
  Float_t rho;
  Float_t Area;
  Float_t nevent;
  Float_t pTleading;
  
  str = Form("%s/recoil.root", wrkdir.Data());
  TFile *foutput = new TFile(str.Data(), "RECREATE");

  Float_t nevents = 1E6;

  for(Int_t itrig = 0; itrig < ntrig; itrig++)
    {
      foutput->cd();
      str = Form("%.1lfpTtrig%.1lf", pTtrigMin[itrig], pTtrigMax[itrig]);
      TH1D *hrecoil = new TH1D(str.Data(), "hrecoil;p_{T, corr}^{recoil jet} (GeV/c)", 400, -200, +200);
      hrecoil->Sumw2();

      str = Form("%.1lfpTtrig%.1lf_pTleading", pTtrigMin[itrig], pTtrigMax[itrig]);
      TH2D *hrecoilpTleading = new TH2D(str.Data(), "hrecoil;p_{T}^{leading} (GeV/c);p_{T, corr}^{recoil jet} (GeV/c)", 400, -200, +200, 400, -200, +200);
      hrecoilpTleading->Sumw2();

      str = Form("%s/%.0lfpTtrig%.0lf/ana_data_jets_R0.4_pTcut0.2.root", wrkdir.Data(), pTtrigMin[itrig], pTtrigMax[itrig]);
      TFile *finput = new TFile(str.Data(), "OPEN");
      TNtuple *ntuple = (TNtuple*)finput->Get("jets");
      ntuple->SetBranchAddress("pT", &pT);
      ntuple->SetBranchAddress("rho", &rho);
      ntuple->SetBranchAddress("Area", &Area);
      ntuple->SetBranchAddress("nevent", &nevent);
      ntuple->SetBranchAddress("pTleading", &pTleading);

      Int_t nentries = ntuple->GetEntries();

      for(Int_t entry = 0; entry < nentries; entry++)
	{
	  ntuple->GetEntry(entry);

	  Double_t pTcorr = pT - rho*Area;
	  
	  hrecoil->Fill(pTcorr);
	  hrecoilpTleading->Fill(pTleading, pTcorr);
 	}

      foutput->cd();

      hrecoil->GetYaxis()->SetTitle("N_{jets}");
      hrecoil->Write();
      hrecoilpTleading->Write();

      hrecoil->Scale(1./(1.2*TMath::Pi()/2.0*nevents), "width");
      str = Form("%s_normalized", hrecoil->GetName());
      hrecoil->GetYaxis()->SetTitle("1/N_{trigger}1/#Delta#phid^{2}N_{jets}/d#etadp_{T} (GeV/c)^{-1}");
      hrecoil->Write(str.Data());

      delete hrecoil;
      delete hrecoilpTleading;

      delete ntuple;
      finput->Close();
      delete finput;
    }
}
