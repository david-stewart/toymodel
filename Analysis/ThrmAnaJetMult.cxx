#include "TH1D.h"
#include "TFile.h"
#include "TNtuple.h"
#include "TString.h"
#include "TSystem.h"
#include "TStopwatch.h"

#include "ThrmAnaJetMult.h"

void ThrmAnaJetMult(TString wrkdir)
{
  TStopwatch timer;
  timer.Start();

  TString str;
  //  TString wrkdir = gSystem->Getenv("WRKDIR");
  
  str = Form("%s/ana_data_jets_R0.4_pTcut0.2.root", wrkdir.Data());
  
  TFile *finput = new TFile(str.Data(), "OPEN");

  TNtuple *ntuple = (TNtuple*)finput->Get("jets");

  str = Form("%s/njets.root", wrkdir.Data());
  TFile *foutput = new TFile(str.Data(), "RECREATE");
  TH1D *hjets = new TH1D("hjets", "hjets;N_{jets};Entries", 20, 0, 20);

  Float_t njet;
  ntuple->SetBranchAddress("njet", &njet);

  Int_t nentries = ntuple->GetEntries();

  Int_t ijet = -1;

  for(Int_t entry = 0; entry < nentries; entry++)
    {
      ntuple->GetEntry(entry);
      if(njet > ijet) ijet = njet;
      else
	{
	  hjets->Fill(ijet);
	  ijet = njet;
	}
    }

  foutput->cd();
  hjets->Write();
  foutput->Close();
  delete foutput;

  finput->Close();
  delete finput;

  timer.Stop();
  timer.Print();
}
