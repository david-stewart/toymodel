#ifndef __ThrmAnaHadronJetData__hh
#define __ThrmAnaHadronJetData__hh

class TH1D;
class TH2D;
class TFile;
class TTree;
class TNtuple;
class TClonesArray;

class ThrmAnaHadronJetData
{
 public:
  ThrmAnaHadronJetData(TString path, Double_t pTcut, Double_t radius);
  ~ThrmAnaHadronJetData();

  void RunAnalysis();

 private:
  void FillJetNtuple(Int_t entry);
  void FillDeltaPtHistos();
  void CreateHistos();

 protected:
  Double_t fradius;
  Double_t frho;

  TH1D *hdpT[6];

  TFile *fjets;
  TFile *foutput;

  TTree *ftree;

  TNtuple *fjettuple;

  TClonesArray *fakt_arr;
  TClonesArray *fembedding_arr;
};

#endif
