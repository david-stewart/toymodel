#ifdef __CINT__
 
#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;

#pragma link C++ function ThrmAnaJetMult;
#pragma link C++ function LHCRecoilJetSpectrum;
#pragma link C++ function RHICRecoilJetSpectrum;

#pragma link C++ class ThrmAnadNdpT;
#pragma link C++ class ThrmAnaJetData;
#pragma link C++ class ThrmAnaJetByJet;
#pragma link C++ class ThrmAnaMakeHistos;
#pragma link C++ class ThrmAnaDeltaPtArea;
#pragma link C++ class ThrmAnaCorrelations;
#pragma link C++ class ThrmAnaHadronJetData;

#endif
