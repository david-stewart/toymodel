#ifndef __JetReconstruction__hh
#define __JetReconstruction__hh

#include "TString.h"
#include "TClonesArray.h"

#include "fjwrapper.h"
#include <vector>

class TFile;
class TTree;
class TClonesArray;

class JetReconstruction
{
 public:
  JetReconstruction(TString wrkdir, Double_t rparam, Double_t pTcut);
  ~JetReconstruction();
  
  void Run();

 private:
  void FillInputVector();
  void RecoJets();
  void RunEmbedding();
  void SaveJets(FJWrapper *reconstruction);
  void Reset();
  void CreateFastJetObjects();

  Int_t FindEmbeddedJet(Double_t &pTreco, Double_t &Area, Double_t &pTleading);

 protected:
  // JET RECO CONFIG
  Double_t frparam;
  Double_t fpTcut;
  
  // FASTJET
  std::vector<fastjet::PseudoJet> kt_jets;
  std::vector<fastjet::PseudoJet> akt_jets;
  std::vector<fastjet::PseudoJet> input_data;
  
  FJWrapper *kt_data;
  FJWrapper *akt_data;
  FJWrapper *akt_data_emb;

  // SAVED INFORMATION
  Double_t frho[3];
  Double_t fsigma[3];
  TClonesArray fktjetsarr;
  TClonesArray faktjetsarr;
  TClonesArray fembeddingarr;
  TClonesArray *fpartarr;

  // I/O
  TFile *finput;
  TFile *foutput;
  TTree *ftreetoy;
  TTree *ftreejets;
};

#endif
