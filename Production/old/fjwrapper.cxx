#include "fjwrapper.h"
#include <cmath>
#include <iostream>
using namespace std;

namespace fj = fastjet;

void FJWrapper::Reset()
{
  area_def = 0;
  vor_area_spec = 0;
  ghosted_area_spec = 0;
  jet_def = 0;
  plugin = 0;
  range = 0;
  clust_seq = 0;
  
  strategy = fj::Best;
  algor = fj::kt_algorithm;
  scheme = fj::E_scheme;

  areatype = fj::active_area;

  ghost_repeat = 1;
  ghost_area = 0.01;
  grid_scatter = 1.0;
  kt_scatter = 1.0;
  mean_ghost_kt = 1e-100;
  maxrap = 2.;

  r = 1.;
  plugin_algor = 0;

  median_used_for_bg = 0;
}

FJWrapper::FJWrapper()
{  
  Reset();
}

FJWrapper::~FJWrapper()
{  
  delete area_def;
  delete vor_area_spec;
  delete ghosted_area_spec;
  delete jet_def;
  delete plugin;
  delete range;
  delete clust_seq;  
}

void FJWrapper::GetMedianAndSigma(double &median, double &sigma, int remove)
{
  if (!clust_seq)
    {
      cerr << "[e] Run the jfinder first" << endl;
    }
  double mean_area = 0;
  try 
    {
      if(!remove)
	clust_seq->get_median_rho_and_sigma(*range, false, median, sigma, mean_area);
      else
	{
	  std::vector<fastjet::PseudoJet> input_jets = sorted_by_pt(clust_seq->inclusive_jets());
	  input_jets.erase(input_jets.begin(), input_jets.begin()+remove);
	  clust_seq->get_median_rho_and_sigma(input_jets, *range, false, median, sigma, mean_area);
	  input_jets.clear();
	}
    }
  
  catch (fj::Error)
    {
      cout << __FUNCTION__ << " [w] FJ Exception caught." << endl;
      median = -1.;
      sigma = -1;
    }
}

int FJWrapper::Run()
{
  if (areatype == fj::voronoi_area)
    {
      //Rfact - check dependence - default is 1.
      fj::VoronoiAreaSpec *vor_area_spec = new fj::VoronoiAreaSpec(1.); 
      area_def = new fj::AreaDefinition(*vor_area_spec);      
    }
  else
    {
      fj::GhostedAreaSpec *ghosted_area_spec = 
	//new fj::GhostedAreaSpec(2.,
	new fj::GhostedAreaSpec(maxrap,
				ghost_repeat, 
				ghost_area, 
				grid_scatter, 
				kt_scatter, 
				mean_ghost_kt);

      area_def = new fj::AreaDefinition(*ghosted_area_spec, 
					areatype);
    }

  //   if (maxrap > 1. - 0.95 * r)
  //     maxrap = 1. - 0.95 * r;

  //range = new fj::RangeDefinition(maxrap);
  range = new fj::RangeDefinition(maxrap - 0.95 * r);
  
  if (algor == fj::plugin_algorithm) 
    {
      if (plugin_algor == 0)
	{
	  // SIS CONE ALGOR
	  double overlap_threshold = 0.75; // this actually splits a lot: thr/min(pt1,pt2)
	  plugin = new fj::SISConePlugin(r, 
					 overlap_threshold,
					 0, //search of stable cones - zero = until no more
					 1.0); // this should be seed effectively for proto jets
	  //__INFO("Define siscone");
	  jet_def = new fastjet::JetDefinition(plugin);
	  //__INFO("SIS cone initialized");
	}
      else
	{
	  cerr << "[e] Unrecognized plugin number!" << endl;
	}
    }
  else
    {
      //__INFO("Define std jet def");
      jet_def = new fj::JetDefinition(algor, 
 				      r,
 				      scheme, 
 				      strategy);
      //jet_def = new fj::JetDefinition(fj::kt_algorithm, 0.4, fj::pt_scheme, fj::Best);
      //__INFO("Jet defined");
    }
  
  try 
    {
      clust_seq = new fj::ClusterSequenceArea(input_particles,
 					      *jet_def,
 					      *area_def);
    }
  catch (fj::Error)
    {
      cout << __FUNCTION__ << " [w] FJ Exception caught." << endl;
      return -1;
    }

  // inclusive jets:
  inclusive_jets.clear();
  inclusive_jets = sorted_by_pt(clust_seq->inclusive_jets(0.0));

  return 0;
}

void FJWrapper::SubtractBackgroundOld(double median_pt)
{
  double median = 0;
  double sigma = 0;
  double mean_area = 0;
  
  try 
    {
      clust_seq->get_median_rho_and_sigma(*range, false, median, sigma, mean_area);
    }

  catch (fj::Error)
    {
      cout << __FUNCTION__ << " [w] FJ Exception caught." << endl;
      if (median_pt < 0)
	{
	  median = -1;
	}
      sigma = -1;
      subtracted_jets.clear();
      return;
    }

  if (median_pt < 0)
    {
      median_pt = median;
    }

  //cout << "median set: " << median_pt << " median from algor " << median << endl;
  median_used_for_bg = median_pt;

  // subtract:
  subtracted_jets.clear();
  for (unsigned i = 0; i < inclusive_jets.size(); i++) 
    {
      // get area of jet i
      double area = clust_seq->area(inclusive_jets[i]);

      // standard subtraction
      double pt_sub = inclusive_jets[i].perp() - median_pt*area;
      double px = pt_sub * sin(inclusive_jets[i].phi());
      double py = pt_sub * cos(inclusive_jets[i].phi());
      double pz = inclusive_jets[i].pz();
      double E  = sqrt(px*px + py*py + pz * pz + inclusive_jets[i].m2());
      fj::PseudoJet jet_sub = inclusive_jets[i];
      jet_sub.reset(px, py, pz, E);
      //if (jet_sub.perp() <= 0 || jet_sub.E() <= 0)
      if (jet_sub.perp2() <= 0 || jet_sub.E() <= 0 || pt_sub < 0)
	{
	  jet_sub.reset(0, 0, 0, 0);	      
	}
      else
	{
	  cout << " phiIn : " << inclusive_jets[i].phi()
	       << " phiOut: " << jet_sub.phi()
	       << endl;
	}
      // 	  cout << " orig " << inclusive_jets[i].perp() 
      // 	       << " diff " << median_pt * area
      // 	       << " subt " << pt_sub 
      // 	       << " subt " << jet_sub.perp()
      // 	       << endl;
      subtracted_jets.push_back(jet_sub); // we keep also empty jets for constistent numbering
    }
}

void FJWrapper::SubtractBackground(double median_pt)
{
  double median = 0;
  double sigma = 0;
  double mean_area = 0;
  
  if (median_pt == 0)
    {
      try 
	{
	  clust_seq->get_median_rho_and_sigma(*range, false, median, sigma, mean_area);
	}
      
      catch (fj::Error)
	{
	  cout << __FUNCTION__ << " [w] FJ Exception caught." << endl;
	  if (median_pt < 0)
	    {
	      median = -1;
	    }
	  sigma = -1;
	  subtracted_jets.clear();
	  return;
	}
      
      if (median_pt < 0)
	{
	  median_pt = median;
	}
      
      //cout << "median set: " << median_pt << " median from algor " << median << endl;
      median_used_for_bg = median_pt;
    }
  else
    {
      median_used_for_bg = median_pt;
    }

  // subtract:
  subtracted_jets.clear();
  for (unsigned i = 0; i < inclusive_jets.size(); i++) 
    {
      //fj::PseudoJet jet_sub = clust_seq->subtracted_jet(inclusive_jets[i], median_used_for_bg);
      //cout << inclusive_jets[i].perp() << " (" << median_used_for_bg << ") -> " << jet_sub.perp() << endl;
      
//       double area = clust_seq->area(inclusive_jets[i]);
//       fj::PseudoJet jet_sub = inclusive_jets[i] - area * median_used_for_bg;

      fj::PseudoJet area4v = clust_seq->area_4vector(inclusive_jets[i]);
      fj::PseudoJet jet_sub = inclusive_jets[i] - area4v * median_used_for_bg;
      subtracted_jets.push_back(jet_sub); // we keep also empty jets for constistent numbering
    }
}

//   if (median_pt < 0)
//     {
//       double median = 0;
//       double sigma = 0;
//       double mean_area = 0;
//       clust_seq->get_median_rho_and_sigma(*range, false, median, sigma, mean_area);
//       fInclusiveJetContainer->SetFeature(median,          TStarJetPicoDefinitions::_EVENT_MEDIAN_PT);
//       fInclusiveJetContainer->SetFeature(sigma,           TStarJetPicoDefinitions::_EVENT_SIGMA);
//       fInclusiveJetContainer->SetFeature(mean_area,       TStarJetPicoDefinitions::_EVENT_MEAN_AREA);

//       median_pt = median;
      
//       clust_seq->get_median_rho_and_sigma(*range, true, median, sigma, mean_area);
//       fInclusiveJetContainer->SetFeature(median,          TStarJetPicoDefinitions::_EVENT_MEDIAN_PT_4VECT);
//       fInclusiveJetContainer->SetFeature(sigma,           TStarJetPicoDefinitions::_EVENT_SIGMA_4VECT);
//       fInclusiveJetContainer->SetFeature(mean_area,       TStarJetPicoDefinitions::_EVENT_MEAN_AREA_4VECT);      
//     }
