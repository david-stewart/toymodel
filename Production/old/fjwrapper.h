#ifndef FJWrapper_HH
#define FJWrapper_HH

#include <vector>
#include <fastjet/PseudoJet.hh>
#include <fastjet/JetDefinition.hh>
#include <fastjet/ClusterSequence.hh>
#include <fastjet/ClusterSequenceArea.hh>
#include <fastjet/AreaDefinition.hh>
#include <fastjet/SISConePlugin.hh>

class FJWrapper
{
 public:
  FJWrapper();
  ~FJWrapper();
  
  int Run();
  void Reset();
  void SubtractBackground(double median_pt = -1);
  void SubtractBackgroundOld(double median_pt = -1);
  void GetMedianAndSigma(double &median, double &sigma, int remove = 0);

  std::vector<fastjet::PseudoJet> input_particles;
  std::vector<fastjet::PseudoJet> inclusive_jets;
  std::vector<fastjet::PseudoJet> subtracted_jets;
  
  fastjet::AreaDefinition        *area_def;
  fastjet::VoronoiAreaSpec       *vor_area_spec;
  fastjet::GhostedAreaSpec       *ghosted_area_spec;
  fastjet::JetDefinition         *jet_def;
  fastjet::JetDefinition::Plugin *plugin;
  fastjet::RangeDefinition       *range;
  fastjet::ClusterSequenceArea   *clust_seq;

  fastjet::Strategy strategy;
  fastjet::JetAlgorithm algor;
  fastjet::RecombinationScheme scheme;

  fastjet::AreaType areatype;

  int    ghost_repeat;
  double ghost_area;
  double grid_scatter;
  double kt_scatter;
  double mean_ghost_kt;
  double maxrap;

  double r;
  int    plugin_algor;

  double median_used_for_bg;
};

#endif
