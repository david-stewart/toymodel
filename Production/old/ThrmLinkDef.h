#ifdef __CINT__
 
#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;

#pragma link C++ function MakeCone;
#pragma link C++ function TestFragLEP;
#pragma link C++ function TestFragIndependent;
#pragma link C++ function MakeFragmentationFunction;

#pragma link C++ class ThrmJet;
#pragma link C++ class ThrmEvent;
#pragma link C++ class ThrmJetReco;
#pragma link C++ class ThrmEmbedding;
#pragma link C++ class ThrmFourVector;
#pragma link C++ class ThrmSimPythiaJet;
#pragma link C++ class ThrmPythiaEmb;
#pragma link C++ class ThrmPythiaTest;
#pragma link C++ class ThrmMemStat;

#endif
