void make_ff()
{
  TStopwatch timer;
  timer.Start();

  MakeFragmentationFunction();

  timer.Stop();
  timer.Print();
}
