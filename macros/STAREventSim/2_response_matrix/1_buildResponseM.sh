#!/bin/bash
source ./set_paths.sh
for RPARAM in 0.2 0.3 0.4 #0.5
do
export RPARAM
JETTYPE="pythia"
#JETTYPE="sp"
CENTRAL=1
if [ $CENTRAL -eq 1 ]; then
SUFF="_central"
PTLEADCUTS="5 6 7"
else
SUFF="_peripheral"
PTLEADCUTS="4 5 6"
fi
export PATH_TO_DELTA_PT_HISTOGRAMS="$TOYMODELDIR/DataOut/$JETTYPE/jet_plus_bg/charged_R${RPARAM}$SUFF"
mkdir -p $PATH_TO_DELTA_PT_HISTOGRAMS/rmatrix


for PTLEAD in `echo $PTLEADCUTS`
do
   export PTLEAD
	root -l buildResponseM.C -q -b
done
done
