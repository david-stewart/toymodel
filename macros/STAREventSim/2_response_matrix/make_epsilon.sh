#!/bin/bash
for CENTRAL in 0 1
do
if [ $CENTRAL -eq 1 ]; then
	SUFFIX=""
	PTLEADCUTS="5 6 7"
else
	SUFFIX="_peripheral"
	PTLEADCUTS="4 5 6"
fi
export SUFFIX

for SYSSUF in  "_AuAu" #"_2u1g" "_m5" "_p5" "_normal" #
do
export SYSSUF

for PRIOR in "powlaw4" 
do
export PRIOR

for RPARAM in 0.2 0.3 0.4 #0.5 
do
export RPARAM

for PTTHRESH in `echo $PTLEADCUTS`
   do
   export PTTHRESH
   root -l -b make_epsilon.C -q
done #pTlead
done #R
done #prior
done #systematics suffix
done
