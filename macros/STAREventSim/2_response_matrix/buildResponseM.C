{
	gSystem->Load("$TOYMODELDIR/Production/libThrm.so");
	gSystem->Load("$TOYMODELDIR/Analysis/libThrmAna.so");
	gSystem->Load("$TOYMODELDIR/Unfolding/libtoyUnfold.so");

	Float_t R=atof(gSystem->Getenv("RPARAM"));
	Float_t pTleading=atof(gSystem->Getenv("PTLEAD"));
	TString path = gSystem->Getenv("PATH_TO_DELTA_PT_HISTOGRAMS");

	UnfoldBuildResponseMatrix *rmatrix = new UnfoldBuildResponseMatrix(path, R, pTleading);
	rmatrix->BuildDeltaPtResponseMatrix();
}
