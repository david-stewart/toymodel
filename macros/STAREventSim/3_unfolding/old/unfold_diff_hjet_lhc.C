void unfold_diff_hjet_lhc(Int_t itrig = 1)
{
  TStopwatch timer;
  timer.Start();
 
  TString str;

  Int_t niterations = 10;

  Int_t nbins = 400;
  Int_t nbinsx;
  Int_t nbinsy;

  Float_t pTminRef = 10.0;
  Float_t pTmaxRef = 15.0;

  Int_t ntrig = 2;
  Float_t pTtrigMin[] = {15.0, 20.0, 25.0};
  Float_t pTtrigMax[] = {20.0, 25.0, 30.0};

  TString data_path = "~/Doutorado/toymodel/data/LHC/hjet/sp/";
  TString prior_path = "~/Doutorado/toymodel/software/Production";
  TString rmatrix_path = "~/Doutorado/toymodel/data/LHC/inclusive/sp/full";

  TF1 *scale = new TF1("scale", "pol0", -50, 0);

  Double_t int_ref = 0.0;
  Double_t int_ana = 0.0;

  //___________________________________________________________________________
  // RESPONSE MATRIX
  str = Form("%s/response_matrix_deltapT_uniform_pTcut0.2.root", rmatrix_path.Data());
  TFile *frmatrix = new TFile(str.Data(), "OPEN");
  TH2D *rmatrix = (TH2D*)frmatrix->Get("hResponse_1E9");

  // MAKE SURE DIM = nbins x nbins
  nbinsx = rmatrix->GetNbinsX();
  nbinsy = rmatrix->GetNbinsY();
  if(nbinsx != nbins && nbinsy != nbins) rmatrix->Rebin2D(nbinsx / nbins, nbinsy / nbins);
  // TRANSPOSING R-MATRIX 
  TH2D *rmtx_temp = (TH2D*)rmatrix->Clone("rmtx_temp");
  rmatrix->Reset("MICE");
  rmatrix->GetXaxis()->SetTitle("p_{T}^{truth} (GeV/c)");
  rmatrix->GetYaxis()->SetTitle("p_{T}^{measured} (GeV/c)");
  for(Int_t binx = 1; binx <= nbins; binx++)
    for(Int_t biny = 1; biny <= nbins; biny++)
      rmatrix->SetBinContent(biny, binx, rmtx_temp->GetBinContent(binx, biny));
  delete rmtx_temp;
  
  //___________________________________________________________________________
  // PRIOR
  // REFERENCE DISTRIBUTION --- TRUTH
  str = Form("%s/recoil_lhc.root", prior_path.Data());
  TFile *fprior = new TFile(str.Data(), "OPEN");
  str = Form("normalized_histo_%.1lfpTtrig%.1lf", pTminRef, pTmaxRef);
  TH1D *htrueref = (TH1D*)fprior->Get(str.Data());

  //___________________________________________________________________________
  // MEASUREMENT
  // REFERENCE DISTRIBUTION --- MEASURED
  str = Form("%s/%.0lfpTtrig%.0lf/histos_jets_R0.4_pTcut0.2.root", data_path.Data(), pTminRef, pTmaxRef);
  TFile *fref = new TFile(str.Data(), "OPEN");
  TH1D *hmeasref = (TH1D*)fref->Get("hDirectSpectrum");

  // ______________________________________________________________________
  // ANALYZED DISTRIBUTION --- MEASURED
  str = Form("%s/%.0lfpTtrig%.0lf/histos_jets_R0.4_pTcut0.2.root", data_path.Data(), pTtrigMin[itrig], pTtrigMax[itrig]);
  TFile *fana = new TFile(str.Data(), "OPEN");
  TH1D *hmeasana = (TH1D*)fana->Get("hDirectSpectrum");
  hmeasana->Sumw2();

  hmeasref->Rebin(10);
  hmeasana->Rebin(10);

  TH1D *hratio = (TH1D*)hmeasana->Clone("hratio");
  hratio->Divide(hmeasref);
  hratio->Fit(scale, "RSN");
  hmeasana->Add(hmeasref, -1.*scale->GetParameter(0));
  delete hratio;

  for(Int_t ibin = 1; ibin < hmeasana->GetNbinsX(); ibin++)
    {
      Double_t pT = hmeasana->GetBinCenter(ibin);
      Double_t yield = hmeasana->GetBinContent(ibin);
      if(pT < 0 || yield < 0)
	{
	  hmeasana->SetBinContent(ibin, 0);
	  hmeasana->SetBinError(ibin, 0);
	}
    }

  //_______________________________________________________________________
  // ANALYZED DISTRIBUTION --- TRUTH
  str = Form("histo_%.1lfpTtrig%.1lf", pTtrigMin[itrig], pTtrigMax[itrig]);
  TH1D *htrueana = (TH1D*)fprior->Get(str.Data());
  TH1D *hprior = (TH1D*)htrueana->Clone("hprior");
  htrueana->Add(htrueref, -1./*scale->GetParameter(0)*/);
  hprior->Sumw2();
  hprior->Reset("MICE");
      
  for(Int_t ibin=1; ibin <= htrueana->GetNbinsX(); ibin++)
    {
      Double_t pT = htrueana->GetBinCenter(ibin);
      if(pT < 5) continue;
      Double_t yield = htrueana->GetBinContent(ibin);
      if(yield <= 0) continue;

      Double_t error = htrueana->GetBinError(ibin);
	  
      hprior->SetBinContent(ibin, yield);
	  
      hprior->SetBinError(ibin, error);
    }
      
  //_______________________________________________________________________
  // UNFOLDING 
  str =  Form("%s/unfolded_difference_%.1lfpTtrig%.1lf.root", data_path.Data(), pTtrigMin[itrig], pTtrigMax[itrig]);
  UnfoldBayes *bayes = new UnfoldBayes(niterations, str.Data());
  bayes->SetHistograms(hmeasana, hprior, rmatrix);
  bayes->Unfold();
  delete bayes;

  timer.Stop();
  timer.Print();
}

