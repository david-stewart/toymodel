void unfold_roounfold_uneqbin()
{
  TStopwatch timer;
  timer.Start();
 
  TString str;
  TString prior_type[]={"truth","flat","pythia","powlaw3","powlaw4","powlaw5","powlaw6","levy","levy_alex"};

  Int_t priorNo= atoi(gSystem->Getenv("PRIOR"));
  TString data_path = gSystem->Getenv("DATA_PATH");
  TString out_dir = gSystem->Getenv("OUT_DIR");
  TString true_path = gSystem->Getenv("TRUE_PATH");
  TString rmatrix_path = gSystem->Getenv("RMATRIX_PATH");
  TString rmatrix_type = gSystem->Getenv("RMATRIX_TYPE");
  Double_t pTcut = atof(gSystem->Getenv("PTCUT"));
  Double_t pTthresh = atof(gSystem->Getenv("PTTHRESH"));
  Double_t R = atof(gSystem->Getenv("RPARAM"));
  Int_t nbins = atoi(gSystem->Getenv("NBINS"));
  Int_t niter = atoi(gSystem->Getenv("NITER")); //number of iterations/kterms
  Int_t efficorr= atoi(gSystem->Getenv("EFFICORR"));//correct the result for jet reconstruction efficiency
  TString epsilon_path = gSystem->Getenv("EPSILON_PATH"); //path to efficiency files
  Int_t doSVD=atoi(gSystem->Getenv("SVD")); //SVD unfolding instead of Bayesian

 
//VARIABLE BINNING
  Float_t pTrange=100; //pTrange in input histograms (has to be same in all histos!)
  const Int_t newbins=30;
  Double_t pTbinArray[newbins];
  pTbinArray[0]=-pTrange;
  Double_t width=20;
  for(int i=1; i<=newbins; i++){
   if(pTbinArray[i-1]==-20) width=10;
   else if(pTbinArray[i-1]==-10) width=5;
   else if(pTbinArray[i-1]==-5) width=2;
   else if(pTbinArray[i-1]==3) width=1;
   else if(pTbinArray[i-1]==10) width=2;
   else if(pTbinArray[i-1]==20) width=5;
   else if(pTbinArray[i-1]==30) width=10;
   else if(pTbinArray[i-1]==50) width=10;
   else if(pTbinArray[i-1]==80) width=20;
   pTbinArray[i]=pTbinArray[i-1]+width;
//if(pTbinArray[i]==pTrange)cout<<"lastbin:"<<i<<endl;
  }
/*cout<<"binning:"<<endl;
for(int i=1; i<=newbins; i++){
cout<<i<<": "<<pTbinArray[i-1]<<" - "<<pTbinArray[i]<<endl;}*/
//return;

TString effsuf="";
if(efficorr)effsuf="_eff";

TString utype="Bayes";
if(doSVD) utype="SVD";
 
//I/O FILES
  str = Form("%s/histos_jets_R%.1lf_pTcut%.1lf.root", data_path.Data(),R, pTcut);
  TFile *finput = new TFile(str.Data(), "OPEN");

str =Form("%s/unfolded_SignalSpectrum_R%.1lf_pTthresh%.1lf.root", out_dir.Data(), R, pTthresh); 
  TFile *fout = new TFile(str.Data(), "RECREATE");



//MEASURED SPECTRUM    
  TH2D *hDSpTleading = (TH2D*)finput->Get("fhDSpTleading");
  Int_t firstbin = hDSpTleading->GetXaxis()->FindBin(pTthresh);
  Int_t lastbin = hDSpTleading->GetNbinsX();
  TH1D *htemp= hDSpTleading->ProjectionY("htemp", firstbin, lastbin);
  Double_t binWidth=htemp->GetBinWidth(1);
  TH1D *hSignalSpectrum = new TH1D("hSignalSpectrum","measured data",newbins,pTbinArray); 
  hSignalSpectrum->Sumw2();
  for(Int_t binx = 1; binx <= htemp->GetNbinsX(); binx++){
	Double_t pT = htemp->GetBinCenter(binx);
	Double_t yield = htemp->GetBinContent(binx);
   Int_t newBin=hSignalSpectrum->FindBin(pT);
   Double_t newWidth=hSignalSpectrum->GetBinWidth(newBin);
	hSignalSpectrum->Fill(pT,yield);
  }
  delete htemp;
//error calculation
  for(Int_t binx = 1; binx <= hSignalSpectrum->GetNbinsX(); binx++){
  	  Double_t yield = hSignalSpectrum->GetBinContent(binx);
	  Double_t error = TMath::Sqrt(yield);
	  hSignalSpectrum->SetBinError(binx, error);
  }
  Double_t int_signal=hSignalSpectrum->Integral();

//Efficiency correction  
TH1D* hepsilon;
if(efficorr){
str = Form("%s/epsilon_R%.1lf_pTlead%0.lf.root",epsilon_path.Data(),R,pTthresh);
TFile *fepsilon= new TFile(str.Data(), "OPEN");
hepsilon=(TH1D*) fepsilon->Get("hepsilon_unfolded");
//hepsilon=(TH1D*) fepsilon->Get("hepsilon_transposed");
}

  //RESPONSE MATRIX, PRIOR
  str = Form("%s/response_matrix_%s_R%.1lf_pTlead%.1lf_prior%i.root", rmatrix_path.Data(),rmatrix_type.Data(),R,pTthresh,priorNo);
  TFile *frmatrix = new TFile(str.Data(), "OPEN");
  TH2D *rmatrix = new TH2D("hresponse","hresponse",newbins,pTbinArray,newbins,pTbinArray);
  TH2D *rmatrix_tmp = (TH2D*)frmatrix->Get("hResponse_1E9");
  TH1D *hMCreco = new TH1D("hmcreco","hmcreco",newbins,pTbinArray);
  TH1D *hMCreco_tmp = (TH1D*)frmatrix->Get("hMCreco_1E9"); //MC measured spectrum
  TH1D *hMCtrue = new TH1D("hmctrue","hmctrue",newbins,pTbinArray);
  TH1D *hMCtrue_tmp = (TH1D*)frmatrix->Get("hMCtrue_1E9"); //MC input spectrum

	rmatrix->Sumw2();
	hMCtrue->Sumw2();
	hMCreco->Sumw2();
/*
  rmatrix->Reset("MICE");
  hMCreco->Reset("MICE");    
  hMCtrue->Reset("MICE"); 
*/

  Int_t nbinsx;
  Int_t nbinsy;
  nbinsx = rmatrix_tmp->GetNbinsX();
  nbinsy = rmatrix_tmp->GetNbinsY();

  rmatrix->GetYaxis()->SetTitle("p_{T}^{truth} (GeV/c)");
  rmatrix->GetXaxis()->SetTitle("p_{T}^{measured} (GeV/c)");
  Double_t binWidthx=rmatrix_tmp->GetXaxis()->GetBinWidth(1); 
  Double_t binWidthy=rmatrix_tmp->GetYaxis()->GetBinWidth(1); 
  for(Int_t binx = 1; binx <= nbinsx; binx++){
    for(Int_t biny = 1; biny <= nbinsy; biny++){
		Double_t yield = rmatrix_tmp->GetBinContent(binx, biny);
		Double_t pTx = rmatrix_tmp->GetBinCenter(binx);
		Double_t pTy = rmatrix_tmp->GetBinCenter(biny);
      Int_t newBinx=rmatrix->GetXaxis()->FindBin(pTx); 
      Int_t newBiny=rmatrix->GetYaxis()->FindBin(pTy);
		Double_t newWidthx=rmatrix->GetXaxis()->GetBinWidth(newBinx); 
		Double_t newWidthy=rmatrix->GetYaxis()->GetBinWidth(newBiny); 
		yield=yield/*(binWidthx/newWidthx)*(binWidthy/newWidthy)*/;
      rmatrix->Fill(pTx,pTy,yield);
	}}
  delete rmatrix_tmp; 
//error calculation
  nbinsx = rmatrix->GetNbinsX();
  nbinsy = rmatrix->GetNbinsY();
  for(Int_t binx = 1; binx <= nbinsx; binx++){ 
  	for(Int_t biny = 1; biny <= nbinsy; biny++){ 
		Double_t yield = rmatrix->GetBinContent(binx, biny); 
		Double_t error = TMath::Sqrt(yield);
		rmatrix->SetBinError(binx, biny, error);
	}}
  // PRIOR
  binWidth=hMCtrue_tmp->GetBinWidth(1);   
  for(Int_t bin = 1; bin <= hMCreco_tmp->GetNbinsX(); bin++)
    {
      Double_t pT = hMCtrue_tmp->GetBinCenter(bin);
      Double_t yield = hMCtrue_tmp->GetBinContent(bin);
		Int_t newBin=hMCtrue->FindBin(pT);  
		Double_t newWidth=hMCtrue->GetBinWidth(newBin);  
      hMCtrue->Fill(pT, yield);
    }
   delete hMCtrue_tmp;
//error calculation
	for(Int_t bin = 1; bin <= hMCtrue->GetNbinsX(); bin++) {
		Double_t yield = hMCtrue->GetBinContent(bin);
		Double_t errorp = TMath::Sqrt(yield);
   	hMCtrue->SetBinError(bin,errorp);
   }

  //SMEARED PRIOR
  binWidth=hMCreco_tmp->GetBinWidth(1);   
  for(Int_t bin = 1; bin <= hMCreco_tmp->GetNbinsX(); bin++)
    {
      Double_t pT = hMCreco_tmp->GetBinCenter(bin);
      Double_t yield = hMCreco_tmp->GetBinContent(bin);
		Int_t newBin=hMCreco->FindBin(pT);  
		Double_t newWidth=hMCreco->GetBinWidth(newBin);  
      hMCreco->Fill(pT, yield);
    }
   delete hMCreco_tmp;
//error calculation
	for(Int_t bin = 1; bin <= hMCreco->GetNbinsX(); bin++) {
		Double_t yield = hMCreco->GetBinContent(bin);
		Double_t errorp = TMath::Sqrt(yield);
   	hMCreco->SetBinError(bin,errorp);
   }

//SAVE INPUT HISTOGRAMS
  fout->mkdir("input");
  fout->cd("input");

  hMCtrue->Write("hprior");
  rmatrix->Write("hresponse");
  //rmatrix->Write("PEC"); //for backward compatibility
  hSignalSpectrum->Write("hmeasured");

//UNFOLDING with RooUnfold
RooUnfoldResponse response (hMCreco,hMCtrue,rmatrix);
for(Int_t iteration=0; iteration<niter; iteration++)
{
cout<<"UNFOLDING, iteration/kterm: "<<iteration<<endl;
if(doSVD){RooUnfoldSvd unfold (&response, hSignalSpectrum, iteration+1);}
else{RooUnfoldBayes   unfoldb (&response, hSignalSpectrum, iteration+1);}
cout<<"WRITING OUTPUT"<<endl;

  //cout<<"integral"<<hReco->Integral()<<endl; 
  //unfold.PrintTable (cout, hSignalSpectrum); 
 
  //TH1D* hReco= (TH1D*) unfold.Hreco(kCovariance/*error calculation method*/); 
     TH1D* hReco;
	if(doSVD) hReco= (TH1D*) unfold.Hreco(3); 
	else hReco= (TH1D*) unfoldb.Hreco(3); 

	if(doSVD) TH1D* hDvec=(TH1D*) unfold.GetDvec();
	//get Covariance Matrix
	if(!doSVD)TMatrixD covM=(TMatrixD) unfoldb.Ereco();
	else TMatrixD covM=(TMatrixD) unfold.Ereco();

	TH2D* hCov=(TH2D*) rmatrix->Clone("hCov");
	hCov->Reset("MICE");
	for(Int_t i=0; i<newbins; i++){
   for(Int_t j=0; j<newbins; j++){
		hCov->SetBinContent(i+1, j+1, covM(i,j));
	}
	}

  fout->mkdir(Form("iter%d", iteration));
  fout->cd(Form("iter%d", iteration));

//BACKFOLDING
TH1D* hbackfold=(TH1D*) hReco->Clone(Form("hbackfold%i",iteration));
hbackfold->Reset("MICE");
hbackfold->SetTitle("backfolded distribution");
hbackfold->Sumw2();
for(int bin=1; bin<=hReco->GetNbinsX(); bin++)
{
   Double_t val=hReco->GetBinContent(bin);
   TH1D* prob=(TH1D*)rmatrix->ProjectionX("prob",bin,bin); //rmatrix has the same binning as hReco
   if(!prob->Integral()>0)continue;
   prob->Scale(1./prob->Integral());
   for(int ev=0; ev<val; ev++)
   {
      Double_t pTnew=prob->GetRandom();
      hbackfold->Fill(pTnew);
   }
/* for(int binb=1; binb<=hReco->GetNbinsX(); binb++)
   {
      double prb=prob->GetBinContent(binb);
      //cout<<"prob: "<<prb<<" val: "<<val<<" pT: "<<hbackfold->GetBinCenter(binb)<<endl;
      hbackfold->Fill(hbackfold->GetBinCenter(binb),prb*val);  
   }*/
delete prob;
}
hbackfold->Write("hbackfolded");
TH1D* hbfmratio=(TH1D*) hbackfold->Clone(Form("hbfmratio%i",iteration));
hbfmratio->Divide(hSignalSpectrum);
hbfmratio->SetTitle("backfolded/measured");
hbfmratio->Write("hbfmratio");



	//EFFICIENCY CORRECTION
	if(efficorr){
	for(int bin=1; bin<=hReco->GetNbinsX(); bin++)
	{
		Double_t oldv=hReco->GetBinContent(bin);
		Int_t efbin=hepsilon->FindBin(hReco->GetBinCenter(bin));
		Double_t eff=hepsilon->GetBinContent(efbin);
		Double_t newv=oldv;
		if(eff>0)newv=oldv/eff;
		hReco->SetBinContent(bin,newv);
	}
	}
  hReco->Write("hunfolded"); 
  hCov->Write("hcovariance"); 
  if(doSVD) hDvec->Write("hdvec");

}//iterations

  fout->Close(); 
  delete fout; 
 
 
  timer.Stop();
  timer.Print();
}

