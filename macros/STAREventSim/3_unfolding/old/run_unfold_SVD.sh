#!/bin/bash
SCRIPT_NAME=`basename -- $0`
_usage() {
    echo "Usage: ${SCRIPT_NAME} RMATRIX_TYPE (BG_sp | BG_dete | dete | effi)" 
    exit 1
}

RMATRIX_TYPE=$1  #BG_sp BG_pyt dete BG_dete - correction for BG (using single particle / pythia jet), detector effects, BG+detector effects
echo "TYPE: $RMATRIX_TYPE"

#check arguments
[ -n "$RMATRIX_TYPE" ] || _usage

COLLIDER="RHIC" # "LHC" # 
JET_TYPE="sp" # sp | pythia
#JET_TYPE="pythia" # sp | pythia
export NBINS=25
#export NBINS=62
# FOR PRIOR DISTRIBUTION
export PTCUT=0.2
export RPARAM=0.2
export EFFICORR=0 #do efficiency correction

if [ $EFFICORR -eq 0 ]; then
EFFSUF=""
else
EFFSUF="_eff"
fi

prior_type=(pp_scaled flat pythia powlaw5 powlaw6 powlaw4 powlaw3)

TOYMODELPATH="/home/rusnak/jet_analysis/toymodel"
WRKDIR="$TOYMODELPATH/DataOut/$JET_TYPE/jet_plus_bg/charged_R${RPARAM}"
export TRUE_PATH="$TOYMODELPATH/DataOut/$JET_TYPE/jetonly/charged_R${RPARAM}"
export PRIOR_PATH="~/jet_analysis/STARJet/out/MB/prior"
export DATA_PATH=$WRKDIR
export RMATRIX_PATH="$WRKDIR/rmatrix"
export RMATRIX_TYPE

#cd "$TOYMODELPATH/macros/unfolding"

for PRIOR in 2 #1 3 5 6 #0: scaled_pp, 1: flat, 2: biased pythia, 3: pT^(-5), 4:pT^(-6) 5: pT^(-7) 6:(pT-2)^(-6)
do
	export PRIOR
	#OUT_DIR=$WRKDIR"/Unfolded_R${RPARAM}_${NBINS}bins/"${prior_type[$PRIOR]}
	OUT_DIR=$WRKDIR"/Unfolded_R${RPARAM}_SVD_${NBINS}bins${EFFSUF}/"${prior_type[$PRIOR]}
   echo "creating directory: $OUT_DIR"
   rm $OUT_DIR/*.root
   mkdir -p $OUT_DIR

	for PTTHRESH in 5.0 #7.0 # 10.0 15.0 20.0 25.0 #
	do
		export PTTHRESH
		for KTERM in 1 2 3 4 5 #6 7 8 9 10
		do
			export KTERM
		   root -b -l -q unfold_SVD.C
	      #root -l -q -b unfold_SVD_uneqbin.C
		done
	done
done
