#!/bin/bash

COLLIDER="RHIC" # "LHC" # 
#export NBINS=200
export NBINS=62
export NITER=5 #number of iterations
# FOR PRIOR DISTRIBUTION
export PTCUT=0.2
export RPARAM=0.4

prior_type=(pp_scaled flat pythia powlaw5 powlaw6 powlaw4 powlaw3)

TOYMODELPATH="/home/rusnak/jet_analysis/toymodel"
WRKDIR="$TOYMODELPATH/DataOut/sp/jet_plus_bg/charged_R${RPARAM}"
export TRUE_PATH="$TOYMODELPATH/DataOut/sp/jetonly/charged_R${RPARAM}"
export PRIOR_PATH="~/jet_analysis/STARJet/out/MB/prior"
export DATA_PATH=$WRKDIR
export RMATRIX_PATH=$WRKDIR

#cd "$TOYMODELPATH/macros/unfolding"

for PRIOR in 0 3 5 6 #0: scaled_pp, 1: flat, 2: biased pythia, 3: pT^(-5), 4:pT^(-6) 5: pT^(-7) 6:(pT-2)^(-6)
do
	export PRIOR
	#OUT_DIR=$WRKDIR"/Unfolded_R${RPARAM}_${NBINS}bins/"${prior_type[$PRIOR]}
	OUT_DIR=$WRKDIR"/Unfolded_R${RPARAM}_Bayes_${NBINS}bins/"${prior_type[$PRIOR]}
   echo "creating directory: $OUT_DIR"
   mkdir -p $OUT_DIR

	for PTTHRESH in 5.0 7.0 # 10.0 15.0 20.0 25.0 #
	do
	  export PTTHRESH
	  root -b -l -q unfold_roounfold_uneqbin.C
	  #root -b -l -q unfold_bayes.C
	  #root -l -q -b unfold_bayes_uneqbin.C
	done
done
