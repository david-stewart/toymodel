void plot_examples(TString example="all")
{
  if(example=="all")
    plot_all();
  else
    plot_example(example);
}

void plot_example(TString example)
{
  TFile *fresults = new TFile("./OutputFiles/examples.root");
  TDirectoryFile *dir = (TDirectoryFile*)fresults->Get(example.Data());

  Double_t xmin = -20;
  Double_t xmax = +40;

  TH1D *htruth = (TH1D*)dir->Get("htruth");
  TH1D *hmeasured = (TH1D*)dir->Get("hmeasured");
  TH1D *hunfolded = (TH1D*)dir->Get("hunfolded");

  TLatex *latex = new TLatex();
  latex->SetNDC();

  htruth->Sumw2();
  hmeasured->Sumw2();
  hunfolded->Sumw2();

  htruth->SetLineColor(kBlack);
  htruth->SetLineWidth(2);

  hmeasured->SetLineColor(kBlue);
  hmeasured->SetMarkerColor(kBlue);
  hmeasured->SetMarkerStyle(kFullCircle);
  
  hunfolded->SetLineColor(kRed);
  hunfolded->SetMarkerColor(kRed);
  hunfolded->SetMarkerStyle(kFullCircle);
  
  TH2D *hresponse = (TH2D*)dir->Get("hresponse");
  TH2D *hcovmatrix = (TH2D*)dir->Get("hcovmatrix");
  TH2D *hunfoldingmatrix = (TH2D*)dir->Get("hunfoldingmatrix");

  TCanvas *cdist = new TCanvas("cdist","cdist");
  cdist->cd();
  cdist->SetGrid();
  cdist->SetLogy();
  htruth->GetXaxis()->SetRangeUser(xmin, xmax);
  htruth->GetYaxis()->SetRangeUser(1, 1E6);
  htruth->GetXaxis()->SetTitle("x");
  htruth->DrawCopy("hist");
  hmeasured->DrawCopy("esame");
  hunfolded->DrawCopy("esame");
  TLegend *legdist = new TLegend(0.6, 0.6, 0.95, 0.9);
  legdist->SetBorderSize(0);
  legdist->SetFillStyle(0);
  legdist->SetMargin(0.12);
  legdist->SetHeader(Form("Truth distribution: %s", example.Data()));
  legdist->AddEntry(htruth, Form("Truth, integral = %.3e", htruth->Integral("width")), "lp");
  legdist->AddEntry(hmeasured, Form("Measured, integral = %.3e", hmeasured->Integral("width")), "lp");
  legdist->AddEntry(hunfolded, Form("Unfolded, integral = %.3ef", hunfolded->Integral("width")), "lp");
  legdist->DrawClone("same");

  TCanvas *cratio = new TCanvas ("cratio", "cratio");
  cratio->cd();
  cratio->SetGrid();
  cratio->SetLogy();
  hunfolded->Divide(htruth);
  hunfolded->GetXaxis()->SetRangeUser(0, xmax);
  hunfolded->GetYaxis()->SetRangeUser(1E-1, 1E+1);
  hunfolded->GetXaxis()->SetTitle("x");
  hunfolded->GetYaxis()->SetTitle("ratio = unfolded/truth");
  hunfolded->DrawCopy("");

  TCanvas *cprior = new TCanvas("cprior","cprior");
  cprior->cd();
  cprior->SetLogz();
  cprior->SetGrid();
  cprior->SetRightMargin(0.15);
  hresponse->GetXaxis()->SetRangeUser(xmin, xmax);
  hresponse->GetYaxis()->SetRangeUser(xmin, xmax);
  hresponse->GetXaxis()->SetTitle("x_{meas}");
  hresponse->GetYaxis()->SetTitle("x_{true}");
  hresponse->GetZaxis()->SetTitle("Entries");
  hresponse->DrawCopy("colz");
  latex->DrawLatex(0.2, 0.8, "Prior distribution");

  TCanvas *ccovmatrix = new TCanvas("ccovmatrix","ccovmatrix");
  ccovmatrix->cd();
  ccovmatrix->SetLogz();
  ccovmatrix->SetGrid();
  ccovmatrix->SetRightMargin(0.15);
  hcovmatrix->GetXaxis()->SetRangeUser(xmin, xmax);
  hcovmatrix->GetYaxis()->SetRangeUser(xmin, xmax);
  hcovmatrix->GetXaxis()->SetTitle("x_{meas}");
  hcovmatrix->GetYaxis()->SetTitle("x_{true}");
  hcovmatrix->GetZaxis()->SetTitle("Entries");
  hcovmatrix->DrawCopy("colz");
  latex->DrawLatex(0.2, 0.8, "Covariance Matrix");

  TCanvas *cunfoldingmatrix = new TCanvas("cunfoldingmatrix","cunfoldingmatrix");
  cunfoldingmatrix->cd();
  cunfoldingmatrix->SetLogz();
  cunfoldingmatrix->SetGrid();
  cunfoldingmatrix->SetRightMargin(0.15);
  hunfoldingmatrix->GetXaxis()->SetRangeUser(xmin, xmax);
  hunfoldingmatrix->GetYaxis()->SetRangeUser(xmin, xmax);
  hunfoldingmatrix->GetXaxis()->SetTitle("x_{meas}");
  hunfoldingmatrix->GetYaxis()->SetTitle("x_{true}");
  hunfoldingmatrix->GetZaxis()->SetTitle("Probability");
  hunfoldingmatrix->DrawCopy("colz");
  latex->DrawLatex(0.2, 0.8, "Unfolding Matrix");
}

void plot_all()
{
  TFile *fresults = new TFile("./OutputFiles/examples.root");

  Double_t xmin = -20;
  Double_t xmax = +40;

  TString str;

  Char_t *example[] = {"delta", "pol1", "pol2", "gaus", "expo"};

  TLatex *latex = new TLatex();
  latex->SetNDC();

  for(Int_t iexample = 0; iexample < 5; iexample++)
  {
    TDirectoryFile *dir = (TDirectoryFile*)fresults->Get(example[iexample]);

    TH1D *htruth = (TH1D*)dir->Get("htruth");
    TH1D *hmeasured = (TH1D*)dir->Get("hmeasured");
    TH1D *hunfolded = (TH1D*)dir->Get("hunfolded");

    htruth->Sumw2();
    hmeasured->Sumw2();
    hunfolded->Sumw2();

    htruth->SetLineColor(kBlack);
    htruth->SetLineWidth(2);

    hmeasured->SetLineColor(kBlue);
    hmeasured->SetMarkerColor(kBlue);
    hmeasured->SetMarkerStyle(kFullCircle);
  
    hunfolded->SetLineColor(kRed);
    hunfolded->SetMarkerColor(kRed);
    hunfolded->SetMarkerStyle(kFullCircle);
  
    TH2D *hresponse = (TH2D*)dir->Get("hresponse");
    TH2D *hcovmatrix = (TH2D*)dir->Get("hcovmatrix");
    TH2D *hunfoldingmatrix = (TH2D*)dir->Get("hunfoldingmatrix");

    TCanvas *cdist = new TCanvas("cdist","cdist");
    cdist->cd();
    cdist->SetGrid();
    cdist->SetLogy();
    htruth->GetXaxis()->SetRangeUser(xmin, xmax);
    htruth->GetYaxis()->SetRangeUser(1, 2E6);
    htruth->GetXaxis()->SetTitle("x");
    htruth->DrawCopy("hist");
    hmeasured->DrawCopy("esame");
    hunfolded->DrawCopy("esame");
    TLegend *legdist = new TLegend(0.55, 0.7, 0.9, 0.9);
    legdist->SetBorderSize(0);
    legdist->SetFillStyle(0);
    legdist->SetMargin(0.12);
    legdist->SetHeader(Form("Truth distribution: %s", example[iexample]));
    legdist->AddEntry(htruth, Form("Truth, integral = %.3e", htruth->Integral("width")), "lp");
    legdist->AddEntry(hmeasured, Form("Measured, integral = %.3e", hmeasured->Integral("width")), "lp");
    legdist->AddEntry(hunfolded, Form("Unfolded, integral = %.3e", hunfolded->Integral("width")), "lp");
    legdist->DrawClone("same");
    latex->DrawLatex(0.12, 0.8, Form("Smearing = Gaus(-1, 2)"));
    
    TCanvas *cratio = new TCanvas ("cratio", "cratio");
    cratio->cd();
    cratio->SetGrid();
    cratio->SetLogy();
    hunfolded->Divide(htruth);
    hunfolded->GetXaxis()->SetRangeUser(0, xmax);
    hunfolded->GetYaxis()->SetRangeUser(1E-1, 1E+1);
    hunfolded->GetXaxis()->SetTitle("x");
    hunfolded->GetYaxis()->SetTitle("ratio = unfolded/truth");
    hunfolded->DrawCopy("");
    latex->DrawLatex(0.2, 0.8, Form("Truth distribution: %s", example[iexample]));

    TCanvas *cprior = new TCanvas("cprior","cprior");
    cprior->cd();
    cprior->SetLogz();
    cprior->SetGrid();
    cprior->SetRightMargin(0.15);
    hresponse->GetXaxis()->SetRangeUser(xmin, xmax);
    hresponse->GetYaxis()->SetRangeUser(xmin, xmax);
    hresponse->GetXaxis()->SetTitle("x_{meas}");
    hresponse->GetYaxis()->SetTitle("x_{true}");
    hresponse->GetZaxis()->SetTitle("Entries");
    hresponse->DrawCopy("colz");
    latex->DrawLatex(0.2, 0.8, "Prior distribution");

    TCanvas *ccovmatrix = new TCanvas("ccovmatrix","ccovmatrix");
    ccovmatrix->cd();
    ccovmatrix->SetLogz();
    ccovmatrix->SetGrid();
    ccovmatrix->SetRightMargin(0.15);
    hcovmatrix->GetXaxis()->SetRangeUser(xmin, xmax);
    hcovmatrix->GetYaxis()->SetRangeUser(xmin, xmax);
    hcovmatrix->GetXaxis()->SetTitle("x_{meas}");
    hcovmatrix->GetYaxis()->SetTitle("x_{true}");
    hcovmatrix->GetZaxis()->SetTitle("Entries");
    hcovmatrix->DrawCopy("colz");
    latex->DrawLatex(0.2, 0.8, "Covariance Matrix");

    TCanvas *cunfoldingmatrix = new TCanvas("cunfoldingmatrix","cunfoldingmatrix");
    cunfoldingmatrix->cd();
    cunfoldingmatrix->SetLogz();
    cunfoldingmatrix->SetGrid();
    cunfoldingmatrix->SetRightMargin(0.15);
    hunfoldingmatrix->GetXaxis()->SetRangeUser(xmin, xmax);
    hunfoldingmatrix->GetYaxis()->SetRangeUser(xmin, xmax);
    hunfoldingmatrix->GetXaxis()->SetTitle("x_{meas}");
    hunfoldingmatrix->GetYaxis()->SetTitle("x_{true}");
    hunfoldingmatrix->GetZaxis()->SetTitle("Probability");
    hunfoldingmatrix->DrawCopy("colz");
    latex->DrawLatex(0.2, 0.8, "Unfolding Matrix");

    str = Form("./Figures/example_%s_dist.png", example[iexample]);
    cdist->SaveAs(str.Data());

    str = Form("./Figures/example_%s_ratio.png", example[iexample]);
    cratio->SaveAs(str.Data());

    str = Form("./Figures/example_%s_prior.png", example[iexample]);
    cprior->SaveAs(str.Data());

    str = Form("./Figures/example_%s_covmatrix.png", example[iexample]);
    ccovmatrix->SaveAs(str.Data());

    str = Form("./Figures/example_%s_unfoldingmatrix.png", example[iexample]);
    cunfoldingmatrix->SaveAs(str.Data());

    delete cdist;
    delete legdist;
    delete cratio;
    delete cprior;
    delete ccovmatrix;
    delete cunfoldingmatrix;
  }
}
