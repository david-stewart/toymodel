void unfold_toymodel_nocut()
{
  TStopwatch timer;
  timer.Start();
 
  TString str;

  Int_t niterations = 5;

  TString data_path = gSystem->Getenv("DATA_PATH");
  TString prior_path = gSystem->Getenv("PRIOR_PATH");
  TString rmatrix_path = gSystem->Getenv("RMATRIX_PATH");

  Double_t pTcut = 0.2;
  
  Int_t nbins = 400;

  str = Form("%s/histos_jets_R0.4_pTcut%.1lf.root", data_path.Data(), pTcut);
  TFile *finput = new TFile(str.Data(), "OPEN");
  
  TH1D *hFullSpectrum = (TH1D*)finput->Get("hDirectSpectrum");
  
  str = Form("%s/response_matrix_deltapT_uniform_pTcut%.1lf.root", rmatrix_path.Data(), pTcut);
  TFile *frmatrix = new TFile(str.Data(), "OPEN");
  TH2D *rmatrix = (TH2D*)frmatrix->Get("hResponse_1E9");

  // MAKE SURE DIM = nbins
  // for time constrain purpose
  Int_t nbinsx;
  nbinsx = hFullSpectrum->GetNbinsX();
  if(nbinsx != nbins) hFullSpectrum->Rebin(nbinsx / nbins);

  nbinsx = hFullSpectrum->GetNbinsX();
  if(nbinsx != nbins) hFullSpectrum->Rebin(nbinsx / nbins);


  // MAKE SURE DIM = nbins x nbins
  Int_t nbinsy;
  nbinsx = rmatrix->GetNbinsX();
  nbinsy = rmatrix->GetNbinsY();
  if(nbinsx != nbins && nbinsy != nbins) rmatrix->Rebin2D(nbinsx / nbins, nbinsy / nbins);
  // TRANSPOSING R-MATRIX 
  TH2D *htemp = (TH2D*)rmatrix->Clone("htemp");
  rmatrix->Reset("MICE");
  rmatrix->GetXaxis()->SetTitle("p_{T}^{truth} (GeV/c)");
  rmatrix->GetYaxis()->SetTitle("p_{T}^{measured} (GeV/c)");
  for(Int_t binx = 1; binx <= nbins; binx++)
    for(Int_t biny = 1; biny <= nbins; biny++)
      rmatrix->SetBinContent(biny, binx, htemp->GetBinContent(binx, biny));
  
  // PRIOR
  TH1D *hprior = (TH1D*)hFullSpectrum->Clone("hprior");
  hprior->SetName("hprior");
  hprior->Reset("MICE");
  hprior->Sumw2();
  
  str = Form("%s/dNdpT.root", prior_path.Data());
  TFile *fprior = new TFile(str.Data(), "OPEN");
  TH1D *hdNdpT = (TH1D*)fprior->Get("hdNdpT_boltz");
  hdNdpT->Sumw2();

  for(Int_t bin = 1; bin <= hdNdpT->GetNbinsX(); bin++)
    {
      Double_t pT = hdNdpT->GetBinCenter(bin);
      Double_t yield = hdNdpT->GetBinContent(bin);
      Double_t error = hdNdpT->GetBinError(bin);

      hprior->Fill(pT, yield);
    }
  
  str = Form("%s", data_path.Data());
  
  UnfoldBayes *bayes = new UnfoldBayes(niterations, Form("%s/unfolded_FullSpectrum_pTcut%.1lf_nocut.root", str.Data(), pTcut));
  bayes->SetHistograms(hFullSpectrum, hprior, rmatrix);
  bayes->Unfold();
  delete bayes;

  timer.Stop();
  timer.Print();
}

