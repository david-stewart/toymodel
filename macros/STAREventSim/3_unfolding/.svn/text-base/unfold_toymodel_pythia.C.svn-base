void unfold_toymodel_pythia()
{
  TStopwatch timer;
  timer.Start();
 
  TString str;

  Int_t niterations = 5;

  TString data_path = gSystem->Getenv("DATA_PATH");
  TString prior_path = gSystem->Getenv("PRIOR_PATH");
  TString rmatrix_path = gSystem->Getenv("RMATRIX_PATH");

  Double_t pTcut = atof(gSystem->Getenv("PTCUT"));
  Double_t pTthresh = atof(gSystem->Getenv("PTTHRESH"));
 
  Int_t nbins = 400;

  str = Form("%s/histos_jets_R0.4_pTcut%.1lf.root", data_path.Data(), pTcut);
  TFile *finput = new TFile(str.Data(), "OPEN");
  
  TH1D *hFullSpectrum = (TH1D*)finput->Get("hDirectSpectrum");
  TH2D *hDSpTleading = (TH2D*)finput->Get("fhDSpTleading");
  Int_t firstbin = hDSpTleading->GetXaxis()->FindBin(pTthresh);
  Int_t lastbin = hDSpTleading->GetNbinsX();
  TH1D *hSignalSpectrum = hDSpTleading->ProjectionY("hSignalSpectrum", firstbin, lastbin);
  hSignalSpectrum->Sumw2();
  
  str = Form("%s/response_matrix_deltapT_uniform_pTcut%.1lf.root", rmatrix_path.Data(), pTcut);
  TFile *frmatrix = new TFile(str.Data(), "OPEN");
  TH2D *rmatrix = (TH2D*)frmatrix->Get("hResponse_1E9");

  // MAKE SURE DIM = nbins
  // for time constrain purpose
  Int_t nbinsx;
  nbinsx = hFullSpectrum->GetNbinsX();
  if(nbinsx != nbins) hFullSpectrum->Rebin(nbinsx / nbins);

  nbinsx = hSignalSpectrum->GetNbinsX();
  if(nbinsx != nbins) hSignalSpectrum->Rebin(nbinsx / nbins);


  // MAKE SURE DIM = nbins x nbins
  Int_t nbinsy;
  nbinsx = rmatrix->GetNbinsX();
  nbinsy = rmatrix->GetNbinsY();
  if(nbinsx != nbins && nbinsy != nbins) rmatrix->Rebin2D(nbinsx / nbins, nbinsy / nbins);
  // TRANSPOSING R-MATRIX 
  TH2D *htemp = (TH2D*)rmatrix->Clone("htemp");
  rmatrix->Reset("MICE");
  rmatrix->GetXaxis()->SetTitle("p_{T}^{truth} (GeV/c)");
  rmatrix->GetYaxis()->SetTitle("p_{T}^{measured} (GeV/c)");
  for(Int_t binx = 1; binx <= nbins; binx++)
    for(Int_t biny = 1; biny <= nbins; biny++)
      rmatrix->SetBinContent(biny, binx, htemp->GetBinContent(binx, biny));
  
  // PRIOR
  TH1D *hprior = (TH1D*)hFullSpectrum->Clone("hprior");
  hprior->SetName("hprior");
  hprior->Reset("MICE");
  
  str = Form("%s/histos_jets_R0.4_pTcut0.2.root", prior_path.Data());
  TFile *fprior = new TFile(str.Data(), "OPEN");
  TH2D *hPtRecpTleadingPrior = (TH2D*)fprior->Get("fhPtRecpTleading");
  Int_t firstbin = hPtRecpTleadingPrior->GetXaxis()->FindBin(pTthresh);
  Int_t lastbin = hPtRecpTleadingPrior->GetNbinsX();
  TH1D *hPtReco = hPtRecpTleadingPrior->ProjectionY("hSignalSpectrum", firstbin, lastbin);
  hPtReco->Sumw2();

  for(Int_t bin = 1; bin <= hPtReco->GetNbinsX(); bin++)
    {
      Double_t pT = hPtReco->GetBinCenter(bin);
      Double_t yield = hPtReco->GetBinContent(bin);
      Double_t error = hPtReco->GetBinError(bin);

      hprior->Fill(pT, yield);
    }
  
  str = Form("%s", data_path.Data());
  
  UnfoldBayes *bayes = new UnfoldBayes(niterations, Form("%s/unfolded_SignalSpectrum_pTcut%.1lf_pTthresh%.1lf.root", str.Data(), pTcut, pTthresh));
  bayes->SetHistograms(hSignalSpectrum, hprior, rmatrix);
  bayes->Unfold();
  delete bayes;

  timer.Stop();
  timer.Print();
}

