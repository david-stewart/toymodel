#!/bin/bash

export BASEDIR=$HOME/toymodel/DataOut

#Mult=(   0 2544 2544 )
#Nbin=( 984    0  984 )

Mult=(    0 8246 8246 )
Nbin=( 1259    0 1259 )
pTcut=( 0.2 1.0 2.0 )

export RADIUS=0.4
export PTTHRESH=4.0
RUNMAX=10

for((RUN=0; RUN < RUNMAX; RUN++))
  do
  for((CENT=2; CENT < 3; CENT++))
    do
    for((ICUT=0; ICUT < 3; ICUT++))
      do
      export PTCUT=${pTcut[ICUT]}
      export WRKDIR=${BASEDIR}/Mult${Mult[CENT]}_Nbin${Nbin[CENT]}/${RUN}
      echo "${WRKDIR}"
      root -l -b make_histos.C -q
    done
  done
done
