void produce_data()
{
  gSystem->Load("~/jet_analysis/toymodel/Production/libThrm.so");
  gSystem->Load("~/jet_analysis/toymodel/Analysis/libThrmAna.so");
  gSystem->Load("~/jet_analysis/toymodel/Unfolding/libtoyUnfold.so");
  TStopwatch timer;
  timer.Start();

  TString outdir = gSystem->Getenv("WRKDIR");
  Int_t useScratch = atoi(gSystem->Getenv("USESCRATCH"));
  if(useScratch) outdir = gSystem->Getenv("SCRATCH");
  TString collider = gSystem->Getenv("COLLIDER");
  Int_t nevts = atof(gSystem->Getenv("NEVENTS"));
  Int_t nbin = atoi(gSystem->Getenv("NBIN"));
  Int_t sigmaNbin = atoi(gSystem->Getenv("SIGMA_NBIN"));
  Int_t mult = atoi(gSystem->Getenv("MULTIPLICITY"));
  Int_t sigmaMult = atoi( gSystem->Getenv("SIGMA_MULTIPLICITY"));
  Int_t jetonly = atoi(gSystem->Getenv("JETONLY"));
  Int_t boltzmann = atoi(gSystem->Getenv("BOLTZMANN"));
  Int_t pythiahard = atoi(gSystem->Getenv("PYTHIAHARDJET"));
  Float_t r=atof(gSystem->Getenv("RADIUS"));

  cout << Form("Nevts = %d \t Mult = %d +/- %d \t Nbin = %d +/- %d", nevts, mult, sigmaMult, nbin, sigmaNbin) << endl;

  ThrmSimulation *sim = new ThrmSimulation(outdir,pythiahard);

  sim->SetNevents(nevts);

  sim->SetNbinaryColl(nbin);
  sim->SetMultiplicity(mult);

  sim->SetSigmaNbinaryColl(sigmaNbin);
  sim->SetSigmaMultiplicity(sigmaMult);

  if(boltzmann) sim->SetThermalOnly();
  if(jetonly) sim->SetJetsOnly();
  //if(pythiahard) sim->SetUsePythJet();
  sim->SetKinematics(collider);
  
  sim->Run();
  
  timer.Stop();
  timer.Print();
}
